//
//  TaskProjectsManager.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/23/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation

class TaskProjectsManager {
    
    private struct Info: Codable {
        var tasks: [String]
        var projects: [String]
    }
    
//    private let kTASK_PROJECT_STORE_FILENAME = "TaskProjectStore.json"
    private let kINFO_DOC_FILENAME = "Info.json"
    
    private var info: Info = Info(tasks: [], projects: []) {
        didSet {
            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(info)
                if let url = createURL(filename: kINFO_DOC_FILENAME) {
                    try localFilesManager.save(fileData: data, url: url)
                }
            } catch {
                print(error)
            }
        }
    }
    
    private var localFilesManager = LocalFilesManager()
    
    init() {
        createBaseDirectory()
        
        if let info = loadInfo() {
            self.info = info
        }
    }
    
    private func createBaseDirectory() {
        if let url = baseFolderURL(), !localFilesManager.directoryExists(url: url) {
            localFilesManager.createDirectory(url: url)
        }
    }
    
    private func baseFolderURL() -> URL? {
        guard let docs = localFilesManager.documentsDirectory() else {
            return nil
        }
        return docs.appendingPathComponent("TaskProjects", isDirectory: true)
    }
    
    private func createURL(filename: String) -> URL? {
        guard let base = baseFolderURL() else {
            return nil
        }
        return base.appendingPathComponent(filename)
    }
    
    private func filenameForID(_ id: String) -> String {
        return id + ".json"
    }
    
    private func loadInfo() -> Info? {
        if let url = createURL(filename: kINFO_DOC_FILENAME) {
            do {
                if let data = try localFilesManager.loadFile(at: url) {
                    let info = try JSONDecoder().decode(Info.self, from: data)
                    return info
                }
            } catch {
                print(error)
            }
        }
        return nil
    }
    
    enum FetchParameters {
        case tasks
        case projects
        case priority
        case all
    }
    
    private enum Error: Swift.Error {
        case noPreviousFile
        case cannotReadFile
    }
    
    func fetch(for parameters: FetchParameters) -> [TaskProject] {
        var ids: [String] = []
        
        switch parameters {
        case .projects:
            ids.append(contentsOf: info.projects)
        case .tasks:
            ids.append(contentsOf: info.tasks)
        default:
            ids.append(contentsOf: info.projects)
            ids.append(contentsOf: info.tasks)
        }
        
        var results: [TaskProject] = []
        
        ids.forEach { (id) in
            if let result = fetch(id: id) {
                results.append(result)
            }
        }
        
        return results
    }
    
    private func fetch(id: String) -> TaskProject? {
        let decoder = JSONDecoder()
        var data: Data?
        
        do {
            if let url = createURL(filename: filenameForID(id)) {
                data = try localFilesManager.loadFile(at: url)
            }
        } catch {
            print(error)
        }
        
        if let data = data {
            do {
                let taskProject = try decoder.decode(TaskProject.self, from: data)
                return taskProject
            } catch {
                print(error)
            }
        }
        
        return nil
    }
    
    func save(taskProjects: [TaskProject]) {
        taskProjects.forEach { (taskProject) in
            save(taskProject: taskProject)
        }
    }
    
    func save(taskProject: TaskProject) {
        switch taskProject.kind {
        case .project:
            if let index = info.tasks.firstIndex(of: taskProject.id) {
                info.tasks.remove(at: index)
            }
            if !info.projects.contains(taskProject.id) {
                info.projects.append(taskProject.id)
            }
        case .task:
            if let index = info.projects.firstIndex(of: taskProject.id) {
                info.projects.remove(at: index)
            }
            if !info.tasks.contains(taskProject.id) {
                info.tasks.append(taskProject.id)
            }
        }
        
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let data = try encoder.encode(taskProject)
            if let url = createURL(filename: filenameForID(taskProject.id)) {
                try localFilesManager.save(fileData: data, url: url)
            }
        } catch {
            print(error)
        }
    }
    
    func delete(taskProject: TaskProject) {
        if taskProject.parentTaskProject != nil {
            return
        }
        
        switch taskProject.kind {
        case .task:
            if let index = info.tasks.firstIndex(of: taskProject.id) {
                info.tasks.remove(at: index)
            }
        case .project:
            if let index = info.projects.firstIndex(of: taskProject.id) {
                info.projects.remove(at: index)
            }
        }
        
        do {
            if let url = createURL(filename: filenameForID(taskProject.id)) {
                try localFilesManager.deleteFile(at: url)
            }
        } catch {
            print(error)
        }
    }
}

//
//  LocalFilesManager.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/20/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation

class LocalFilesManager {
        
    enum Error: Swift.Error {
        case writingFailed
        case loadingFailed
        case deletingFailed
    }
    
    func documentsDirectory() -> URL? {
        let fileManager = FileManager.default
        guard let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        return url
    }
    
    func save(fileData: Data, url: URL) throws {
        do {
            try fileData.write(to: url)
        } catch {
            throw Error.writingFailed
        }
        
    }
    
    func loadFile(at url: URL) throws -> Data? {
        do {
            let data = try Data(contentsOf: url)
            return data
        } catch {
            print(error)
            throw Error.loadingFailed
        }
    }
    
    func deleteFile(at url: URL) throws {
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            print(error)
            throw Error.deletingFailed
        }
    }
    
    func createDirectory(url: URL) {
        do {
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: false, attributes: nil)
        } catch {
            print(error)
        }
    }
    
    func directoryExists(url: URL) -> Bool {
        // TODO: FIX THIS - fileExists doesn't check accurately
        var isDir: ObjCBool = false
        if FileManager.default.fileExists(atPath: url.path, isDirectory: &isDir) {
            if isDir.boolValue {
                return true
            }
        }
        return false
    }
}

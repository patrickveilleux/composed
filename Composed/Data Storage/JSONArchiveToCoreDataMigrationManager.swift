//
//  JSONArchiveToCoreDataMigrationManager.swift
//  Composed
//
//  Created by Patrick Veilleux on 6/24/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class JSONArchiveToCoreDataMigrationManager {
    
    static let shared: JSONArchiveToCoreDataMigrationManager = JSONArchiveToCoreDataMigrationManager(appDelegate: UIApplication.shared.delegate as! AppDelegate)
    
    private let appDelegate: AppDelegate
    private let moc: NSManagedObjectContext
    
    private init(appDelegate: AppDelegate) {
        self.appDelegate = appDelegate
        self.moc = appDelegate.persistentContainer.viewContext
    }
    
    func migrate() {
        let taskProjectsManager = TaskProjectsManager()
        let taskProjects = taskProjectsManager.fetch(for: .all)
        
        taskProjects.forEach { (taskProject) in
            _ = createTaskProjectMO(from: taskProject)
        }
        
        appDelegate.saveContext()
        UserDefaults.standard.set(true, forKey: "DidMigrateToCoreData")
    }
    
    private func createTaskProjectMO(from taskProject: TaskProject) -> TaskProjectMO? {
        var children = [TaskProjectMO]()
        taskProject.subTaskProjects.forEach { (subTaskProject) in
            if let newChild = createTaskProjectMO(from: subTaskProject) {
                children.append(newChild)
            }
        }
        
        if let entity = checkForEntity(id: taskProject.id) {
            entity.addToChildren(NSSet(array: children))
            return nil
        }
        
        guard let newItem = NSEntityDescription.insertNewObject(forEntityName: "TaskProject", into: moc) as? TaskProjectMO else {
            return nil
        }
        
        newItem.dateCreated = taskProject.dateCreated
        newItem.dueDate = taskProject.dueDate
        newItem.id = UUID(uuidString: taskProject.id) ?? UUID()
        newItem.notes = taskProject.notes ?? ""
        newItem.title = taskProject.title
        newItem.taskComplete = taskProject.isComplete
        
        newItem.addToChildren(NSSet(array: children))
        
        return newItem
    }
    
    private func checkForEntity(id: String) -> TaskProjectMO? {
        guard let uuid = UUID(uuidString: id) else { return nil }
        
        let fetch: NSFetchRequest = TaskProjectMO.fetchRequest()
        fetch.predicate = NSPredicate(format: "id == %@", uuid as CVarArg)
        
        do {
            let taskProjects = try moc.fetch(fetch)
            return taskProjects.count > 0 ? taskProjects.first! : nil
        } catch {
            print(error)
        }
        return nil
    }
}

//
//  TaskProjectsHomeViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/8/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import CoreData

class TaskProjectsHomeViewController: TaskProjectsViewController {

    private let kTASK_PROJECT_DETAILS_SEGUE_ID = "TaskProjectDetails"
    private let kTASK_PROJECT_DETAILS_STORYBOARD_ID = "TaskProjectDetails"
    
    private let kUSER_DEFAULTS_PROJECT_SORT_METHOD = "ProjectSortMethod"
    private let kUSER_DEFAULTS_TASK_SORT_METHOD = "TaskSortMethod"
    
    private var menuButton: UIButton!
    private var noTasksLabel: UILabel!
    private var noTasksLabelAnimationDuration: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        setupMenuButton()
        setupNoTasksLabel()
        
        taskProjectsTableVC.projectSortMethod = getProjectSortMethodFromUserDefaults() ?? .onScheduleBackwards
        taskProjectsTableVC.taskSortMethod = getTaskSortMethodFromUserDefaults() ?? .completedTrueLast
        
        taskProjectsTableVC.$projectSortMethod
            .sink { [unowned self] sortMethod in
                self.saveProjectSortMethodToUserDefaults(sortMethod)
            }.store(in: &subscriptions)
        
        taskProjectsTableVC.$taskSortMethod
            .sink { [unowned self] sortMethod in
                self.saveTaskSortMethodToUserDefaults(sortMethod)
            }.store(in: &subscriptions)
    }
    
    private func setupMenuButton() {
        if menuButton == nil {
            menuButton = UIButton()
            menuButton.tintColor = .label
            menuButton.setImage(UIImage(systemName: "ellipsis.circle"), for: .normal)
            let symbolConfig = UIImage.SymbolConfiguration(pointSize: 24, weight: .light)
            menuButton.setPreferredSymbolConfiguration(symbolConfig, forImageIn: .normal)
            view.addSubview(menuButton)
            
            menuButton.addTarget(self, action: #selector(presentMenu), for: .touchUpInside)
            
            menuButton.translatesAutoresizingMaskIntoConstraints = false
            tableViewConstraintTop.isActive = false
            NSLayoutConstraint.activate([
                menuButton.heightAnchor.constraint(equalToConstant: 44),
                menuButton.widthAnchor.constraint(equalToConstant: 44),
                menuButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
                menuButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -13),
                taskProjectsTableVC.view.topAnchor.constraint(equalTo: menuButton.bottomAnchor, constant: 0)
            ])
        }
        
        let headerLabel = UILabel()
        headerLabel.font = UIFont.systemFont(ofSize: 24, weight: .medium)
        headerLabel.text = "Home"
        
        let image = UIImageView(image: UIImage(systemName: "house"))
        let symbolConfig = UIImage.SymbolConfiguration(pointSize: 24, weight: .light)
        image.preferredSymbolConfiguration = symbolConfig
        image.tintColor = .label
        image.contentMode = .center
        
        let hStack = UIStackView(arrangedSubviews: [image])
        hStack.axis = .horizontal
        hStack.spacing = 8
        view.addSubview(hStack)
        
        hStack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            hStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            hStack.heightAnchor.constraint(equalToConstant: 44),
            hStack.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    private func setupNoTasksLabel() {
        if noTasksLabel == nil {
            noTasksLabel = UILabel()
            noTasksLabel.font = UIFont.preferredFont(forTextStyle: .callout)
            noTasksLabel.textColor = Appearance.Color.gray
            noTasksLabel.numberOfLines = 0
            noTasksLabel.textAlignment = .center
            noTasksLabel.text = "Get started by tapping on the blue, plus button to create a task."
            
            taskProjectsTableVC.$numberOfTaskProjects
                .removeDuplicates()
                .sink { count in
                    RunLoop.main.schedule {
                        UIView.animate(withDuration: self.noTasksLabelAnimationDuration, animations: {
                            if count == 0 {
                                self.noTasksLabel.alpha = 1
                                self.taskProjectsTableVC.tableView.alpha = 0
                            } else {
                                self.noTasksLabel.alpha = 0
                                self.taskProjectsTableVC.tableView.alpha = 1
                            }
                        }) { _ in
                            self.noTasksLabelAnimationDuration = 0.3
                        }
                    }
                }.store(in: &subscriptions)

            view.addSubview(noTasksLabel)
            noTasksLabel.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                noTasksLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                noTasksLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
                noTasksLabel.heightAnchor.constraint(equalToConstant: 100),
                noTasksLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0)
            ])
        }
    }
    
    private func getProjectSortMethodFromUserDefaults() -> TaskProjectSortMethod.Project? {
        guard let int = UserDefaults.standard.value(forKey: kUSER_DEFAULTS_PROJECT_SORT_METHOD) as? Int16 else { return nil }
        return TaskProjectSortMethod.Project(rawValue: int)
    }
    
    private func getTaskSortMethodFromUserDefaults() -> TaskProjectSortMethod.Task? {
        guard let int = UserDefaults.standard.value(forKey: kUSER_DEFAULTS_TASK_SORT_METHOD) as? Int16 else { return nil }
        return TaskProjectSortMethod.Task(rawValue: int)
    }
    
    private func saveProjectSortMethodToUserDefaults(_ sortMethod: TaskProjectSortMethod.Project) {
        UserDefaults.standard.set(sortMethod.rawValue, forKey: kUSER_DEFAULTS_PROJECT_SORT_METHOD)
    }
    
    private func saveTaskSortMethodToUserDefaults(_ sortMethod: TaskProjectSortMethod.Task) {
        UserDefaults.standard.set(sortMethod.rawValue, forKey: kUSER_DEFAULTS_TASK_SORT_METHOD)
    }
    
    @objc private func presentMenu() {
        let menuVC = UINavigationController(rootViewController: MenuViewController())
        present(menuVC, animated: true)
    }
    
    private func presentDetailViewController(for taskProject: TaskProjectMO) {
        if let vc = storyboard?.instantiateViewController(identifier: kTASK_PROJECT_DETAILS_STORYBOARD_ID) as? TaskProjectDetailsViewController {
            vc.taskProject = taskProject
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func didTapFloatingActionButton(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let moc = appDelegate.persistentContainer.viewContext
        
        guard let taskProject = NSEntityDescription.insertNewObject(forEntityName: TaskProjectMO.entityName, into: moc) as? TaskProjectMO else { return }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        presentDetailViewController(for: taskProject)
    }
    
    override func didSelectTaskProject(_ taskProject: TaskProjectMO) {
        presentDetailViewController(for: taskProject)
    }
}

extension TaskProjectsHomeViewController: PopNotifiable {
    func willPopHere() {
        self.taskProjectsTableVC.reloadTable()
    }
}

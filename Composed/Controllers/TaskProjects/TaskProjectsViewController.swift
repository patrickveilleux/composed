//
//  TaskProjectsViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/16/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine
import CoreData

class TaskProjectsViewController: UIViewController {
    
    var taskProjectsTableVC: TaskProjectsTableViewController!
    var floatingActionButton: FloatingActionButton!
    var tableViewConstraintTop: NSLayoutConstraint!
    
    var subscriptions = Set<AnyCancellable>()
    
    var taskRequest: NSFetchRequest<TaskProjectMO> {
        let sortDescriptor = NSSortDescriptor(key: #keyPath(TaskProjectMO.title), ascending: true)
        let request: NSFetchRequest<TaskProjectMO> = TaskProjectMO.fetchRequest()
        request.predicate = NSPredicate(format: "parent == nil AND kind == 0")
        request.sortDescriptors = [sortDescriptor]
        return request
    }
    
    var projectRequest: NSFetchRequest<TaskProjectMO> {
        let sortDescriptor = NSSortDescriptor(key: #keyPath(TaskProjectMO.title), ascending: true)
        let request: NSFetchRequest<TaskProjectMO> = TaskProjectMO.fetchRequest()
        request.predicate = NSPredicate(format: "parent == nil AND kind == 1")
        request.sortDescriptors = [sortDescriptor]
        return request
    }
    
    var backgroundColor: UIColor = .systemBackground
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = backgroundColor
                
        setupTaskProjectsTableVC()
        setupFloatingActionButton()
    }
    
    private func setupFloatingActionButton() {
        if floatingActionButton == nil {
            floatingActionButton = FloatingActionButton(frame: .zero)
            let config = UIImage.SymbolConfiguration(pointSize: 20, weight: .medium)
            floatingActionButton.setImage(UIImage(systemName: "plus", withConfiguration: config), for: .normal)
            view.addSubview(floatingActionButton)
            
            floatingActionButton.addTarget(self, action: #selector(didTapFloatingActionButton(_:)), for: .touchUpInside)
            
            floatingActionButton.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                floatingActionButton.widthAnchor.constraint(equalToConstant: 44),
                floatingActionButton.heightAnchor.constraint(equalToConstant: 44),
                floatingActionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
                floatingActionButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
            ])
        }
    }
    
    private func setupTaskProjectsTableVC() {
        if taskProjectsTableVC == nil {
            taskProjectsTableVC = TaskProjectsTableViewController(taskRequest: taskRequest, projectRequest: projectRequest)
            taskProjectsTableVC.backgroundColor = backgroundColor
            
            taskProjectsTableVC.publisher.sink { (action) in
                switch action {
                case let .didSelectTaskProject(taskProject):
                    self.didSelectTaskProject(taskProject)
                }
            }.store(in: &subscriptions)

            addChild(taskProjectsTableVC)
            view.insertSubview(taskProjectsTableVC.view, at: 0)

            taskProjectsTableVC.view.translatesAutoresizingMaskIntoConstraints = false
            let guide = view.safeAreaLayoutGuide
            tableViewConstraintTop = taskProjectsTableVC.view.topAnchor.constraint(equalTo: guide.topAnchor, constant: 0)
            NSLayoutConstraint.activate([
                tableViewConstraintTop,
                taskProjectsTableVC.view.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 0),
                taskProjectsTableVC.view.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0),
                taskProjectsTableVC.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
            ])
        }
    }
    
    /// Creates an override point to intercept this action
    @objc func didTapFloatingActionButton(_ sender: Any) {
    }
    
    /// Creates an override point to intercept this action
    func didSelectTaskProject(_ taskProject: TaskProjectMO) {
    }
}

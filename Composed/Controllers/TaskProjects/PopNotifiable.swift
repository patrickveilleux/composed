//
//  PopNotifiable.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/15/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation

protocol PopNotifiable {
    func willPopHere()
}

//
//  GuideViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 4/29/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

class GuideViewController: UIViewController {
    
    var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 31, green: 31, blue: 31)
        configureScrollView()
    }
    
    private func configureScrollView() {
        if scrollView == nil {
            scrollView = UIScrollView()
            scrollView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(scrollView)
            
            NSLayoutConstraint.activate([
                scrollView.topAnchor.constraint(equalTo: view.topAnchor),
                scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
            
            let image = UIImage(named: "Guide")!
            let imageRatio = image.size.height / image.size.width
            
            let imageView = UIImageView(image: image)
            imageView.contentMode = .scaleAspectFit
            imageView.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addSubview(imageView)
            
            NSLayoutConstraint.activate([
                imageView.topAnchor.constraint(equalTo: scrollView.topAnchor),
                imageView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
                imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: imageRatio)
            ])
            
            let dismissButton = UIButton()
            dismissButton.backgroundColor = Appearance.Color.blue
            dismissButton.setTitle("Get Started", for: .normal)
            dismissButton.setTitleColor(.white, for: .normal)
            dismissButton.titleLabel?.font = UIFont.preferredFont(for: .headline, weight: .bold)
            dismissButton.layer.cornerRadius = 22
            dismissButton.layer.masksToBounds = true
            dismissButton.addTarget(self, action: #selector(didTapDismissButton(_:)), for: .touchUpInside)
            dismissButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
            dismissButton.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addSubview(dismissButton)
            
            NSLayoutConstraint.activate([
                dismissButton.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 8),
                dismissButton.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 20),
                dismissButton.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -20),
                dismissButton.heightAnchor.constraint(equalToConstant: 44),
                dismissButton.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20)
            ])
            
            print("Image view size \(scrollView.bounds)")
//            imageView.layoutIfNeeded()
        }
    }
    
    @objc func didTapDismissButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

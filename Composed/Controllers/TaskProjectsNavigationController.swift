//
//  TaskProjectsNavigationController.swift
//  Composed
//
//  Created by Patrick Veilleux on 4/21/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

final class TaskProjectsNavigationController: UINavigationController {

    private var duringPushAnimation = false
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactivePopGestureRecognizer?.delegate = self
    }
    
    deinit {
        delegate = nil
        interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        duringPushAnimation = true
        
        super.pushViewController(viewController, animated: animated)
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        let popped = super.popViewController(animated: animated)
        if let vc = topViewController as? PopNotifiable {
            vc.willPopHere()
        }
        return popped
    }
    
    override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        let popped = super.popToRootViewController(animated: animated)
        if let vc = topViewController as? PopNotifiable {
            vc.willPopHere()
        }
        return popped
    }
    
    override func popToViewController(_ viewController: UIViewController, animated: Bool) -> [UIViewController]? {
        let popped = super.popToViewController(viewController, animated: animated)
        if let vc = topViewController as? PopNotifiable {
            vc.willPopHere()
        }
        return popped
    }
}


// MARK: - UINavigationControllerDelegate

extension TaskProjectsNavigationController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let taskProjectsNavigationController = navigationController as? TaskProjectsNavigationController else {
            return
        }
        
        taskProjectsNavigationController.duringPushAnimation = false
    }
}


// MARK: - UIGestureRecognizerDelegate

extension TaskProjectsNavigationController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer == interactivePopGestureRecognizer else {
            return true
        }
        
        // Disable pop gesture in two situations:
        // 1) when the pop animation is in progress
        // 2) when user swipes quickly a couple of times and animations don't have time to be performed
        return viewControllers.count > 1 && duringPushAnimation == false
    }
}

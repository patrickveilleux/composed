//
//  TaskProjectDetailsViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/17/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine
import CoreData

class TaskProjectDetailsViewController: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var parentTaskProjectButton: UIButton!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var dueDateButton: DueDateButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var scrollViewContent: UIView!
    
    private var notesViewController: TaskProjectDetailsNotesViewController!
    private var tasksViewController: TaskProjectDetailsTaskProjectsViewController!
    private var notificationsViewController: TaskProjectDetailsNotificationsViewController!
    
    private let kTASK_PROJECT_DETAILS_STORYBOARD_ID = "TaskProjectDetails"
    
    var taskProject: TaskProjectMO!
    var nestedLayer: Int = 0
        
    var subscriptions = Set<AnyCancellable>()
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = taskProject.title
        
        let interaction = UIContextMenuInteraction(delegate: self)
        backButton.addInteraction(interaction)
        backButton.imageView?.contentMode = .scaleAspectFit
        
        setupTitleTextFieldChangeListener()
        setupDueDateButton()
        setupScrollView()
        configureForModel()
                
        navigationController?.interactivePopGestureRecognizer?.addTarget(self, action: #selector(handlePopGesture(gesture:)))
    }
    
    private func configureForModel() {
        var text = ""
        if let parent = taskProject.parent?.title, let root = taskProject.rootTaskProject?.title {
            if parent != root {
                text = root
                for _ in 1..<self.nestedLayer {
                    text.append("/")
                }
            }
            text.append(parent)
        }
        parentTaskProjectButton.setTitle(text, for: .normal)
        
        taskProject.publisher(for: \.title)
            .map({ $0 })
            .assign(to: \.text, on: titleTextField)
            .store(in: &subscriptions)
        
        taskProject.publisher(for: \.dueDate)
            .sink { dueDate in
                if let date = dueDate {
                    let formatter = DateFormatter()
                    formatter.doesRelativeDateFormatting = true
                    formatter.dateStyle = .medium
                    formatter.timeStyle = .short
                    self.dueDateButton.setTitle(formatter.string(from: date), for: .normal)
                    self.dueDateButton.setTitleColor(.label, for: .normal)
                } else {
                    self.dueDateButton.setTitle("Set Due Date", for: .normal)
                    self.dueDateButton.setTitleColor(Appearance.Color.gray, for: .normal)
                }
            }.store(in: &subscriptions)
        
        segmentedControl.selectedSegmentIndex = (!taskProject.notes.isEmpty && taskProject.children.count == 0) ? 1 : 0
        moveToDetailController(segmentedControl.selectedSegmentIndex)
        
        if titleTextField.text?.isEmpty ?? true {
            titleTextField.becomeFirstResponder()
        }
    }
    
    private func setupTitleTextFieldChangeListener() {
        NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: titleTextField)
            .map { ($0.object as! UITextField).text }
            .removeDuplicates()
            .sink { (text) in
                self.taskProject.title = text ?? ""
        }.store(in: &subscriptions)
    }
    
    private func setupDueDateButton() {
        dueDateButton.publisher
            .sink { [weak self] action in
                switch action {
                case .dateChanged(let date):
                    self?.taskProject.dueDate = date
                case .didTapDoneButton:
                    self?.dueDateButton.resignFirstResponder()
                case .didTapDeleteButton:
                    self?.taskProject.dueDate = nil
                    self?.dueDateButton.resignFirstResponder()
                }
            }.store(in: &subscriptions)
    }
    
    private func setupScrollView() {
        loadNotesVC()
        loadTasksVC()
        loadNotificationsVC()
    }
    
    private func loadNotesVC() {
        if notesViewController == nil {
            notesViewController = TaskProjectDetailsNotesViewController(model: TaskProjectDetailsNotesViewModel(notes: taskProject.notes))
            notesViewController.view.backgroundColor = Appearance.Color.nearWhite
            notesViewController.view.layer.cornerRadius = Appearance.cornerRadius
            notesViewController.view.layer.masksToBounds = true
            
            notesViewController.model.$notes.sink { (text) in
                self.taskProject.notes = text
            }.store(in: &subscriptions)
            
            addChild(notesViewController)
            notesViewController.didMove(toParent: self)
            
            let notesView = notesViewController.view!
            scrollViewContent.addSubview(notesView)
            notesView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                notesView.topAnchor.constraint(equalTo: scrollViewContent.topAnchor, constant: 20),
                notesView.leadingAnchor.constraint(equalTo: scrollViewContent.leadingAnchor, constant: view.bounds.width + 20),
                notesView.bottomAnchor.constraint(equalTo: scrollViewContent.bottomAnchor, constant: -20),
                notesView.widthAnchor.constraint(equalToConstant: view.bounds.width - 40)
            ])
        }
    }
    
    private func loadTasksVC() {
        if tasksViewController == nil {
            tasksViewController = TaskProjectDetailsTaskProjectsViewController(parentTaskProject: taskProject)
            tasksViewController.backgroundColor = Appearance.Color.nearWhite
            tasksViewController.view.layer.cornerRadius = Appearance.cornerRadius
            tasksViewController.view.layer.masksToBounds = true
            
            tasksViewController.publisher.sink { (action) in
                switch action {
                case .didSelectTaskProject(let taskProject):
                    self.presentDetailViewController(for: taskProject)
                case .addNewTaskProject:
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let moc = appDelegate.persistentContainer.viewContext
                    
                    guard let newItem = NSEntityDescription.insertNewObject(forEntityName: TaskProjectMO.entityName, into: moc) as? TaskProjectMO else { return }
                    self.taskProject.addToChildren(newItem)
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    
                    self.presentDetailViewController(for: newItem)
                }
            }.store(in: &subscriptions)
            
            addChild(tasksViewController)
            tasksViewController.didMove(toParent: self)
            
            let tasksView = tasksViewController.view!
            scrollViewContent.addSubview(tasksView)
            tasksView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                tasksView.topAnchor.constraint(equalTo: scrollViewContent.topAnchor, constant: 20),
                tasksView.leadingAnchor.constraint(equalTo: scrollViewContent.leadingAnchor, constant: 20),
                tasksView.bottomAnchor.constraint(equalTo: scrollViewContent.bottomAnchor, constant: -20),
                tasksView.widthAnchor.constraint(equalToConstant: view.bounds.width - 40)
            ])
        }
    }
    
    private func loadNotificationsVC() {
        if notificationsViewController == nil {
            let notificationsVCModel = TaskProjectDetailsNotificationsViewModel(taskProject: taskProject)
            notificationsViewController = TaskProjectDetailsNotificationsViewController(model: notificationsVCModel)
            notificationsViewController.view.backgroundColor = Appearance.Color.nearWhite
            notificationsViewController.view.layer.cornerRadius = Appearance.cornerRadius
            notificationsViewController.view.layer.masksToBounds = true
            
            addChild(notificationsViewController)
            notificationsViewController.didMove(toParent: self)
            
            let notificationsView = notificationsViewController.view!
            scrollViewContent.addSubview(notificationsView)
            notificationsView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                notificationsView.topAnchor.constraint(equalTo: scrollViewContent.topAnchor, constant: 20),
                notificationsView.leadingAnchor.constraint(equalTo: scrollViewContent.leadingAnchor, constant: 2 * view.bounds.width + 20),
                notificationsView.bottomAnchor.constraint(equalTo: scrollViewContent.bottomAnchor, constant: -20),
                notificationsView.widthAnchor.constraint(equalToConstant: view.bounds.width - 40)
            ])
        }
    }
    
    private func presentDetailViewController(for taskProject: TaskProjectMO) {
        if let vc = storyboard?.instantiateViewController(identifier: kTASK_PROJECT_DETAILS_STORYBOARD_ID) as? TaskProjectDetailsViewController {
            vc.taskProject = taskProject
            vc.nestedLayer = nestedLayer + 1
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - Interactions
    
    @IBAction func didTapParentTaskProjectButton(_ sender: Any) {
        if let count = navigationController?.viewControllers.count, count >= 2 {
            if let rootTaskProjectViewController = navigationController?.viewControllers[1] {
                navigationController?.popToViewController(rootTaskProjectViewController, animated: true)
            }
        }
    }
    
    @IBAction func didTapDueDateButton(_ sender: UIButton) {
        let datePicker = DatePickerViewController(title: "Select Due Date", message: nil, preferredStyle: .actionSheet)
        datePicker.datePicker.minimumDate = Date().addingTimeInterval(60)
        let date = taskProject.dueDate ?? Date().addingTimeInterval(60)
        datePicker.datePicker.date = date.zeroSeconds!.timeIntervalSinceNow < 0 ? Date().addingTimeInterval(60) : date
        
        let done = UIAlertAction(title: "Done", style: .cancel) { _ in
            self.taskProject.dueDate = datePicker.datePicker.date
        }
        datePicker.addAction(done)
        
        let delete = UIAlertAction(title: "Delete", style: .destructive) { _ in
            self.taskProject.dueDate = nil
        }
        datePicker.addAction(delete)
        
        present(datePicker, animated: true)
    }
    
    @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        moveToDetailController(sender.selectedSegmentIndex)
    }
    
    private func moveToDetailController(_ index: Int) {
        let offset = CGPoint(
            x: containerScrollView.bounds.width * CGFloat(index),
            y: 0)
        containerScrollView.setContentOffset(offset, animated: true)
        
        if index == 2 {
            checkNotificationAuthorizationStatus()
        }
    }
    
    private func checkNotificationAuthorizationStatus() {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .notDetermined:
                center.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
                    if granted {
                        self.enableNotifications()
                    }
                    
                    if let error = error {
                        print(error)
                    }
                }
            case .authorized, .provisional:
                self.enableNotifications()
            case .denied:
                self.disableNotifications()
            default:
                break
            }
        }
    }
    
    private func enableNotifications() {
        notificationsViewController.notificationsAreEnabled = true
    }
    
    private func disableNotifications() {
        notificationsViewController.notificationsAreEnabled = false
    }
    
    private func prepareForPop() {
        view.endEditing(true)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.saveContext()
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        prepareForPop()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func handlePopGesture(gesture: UIGestureRecognizer) {
        if gesture.state == .began {
            prepareForPop()
        }
    }
}

extension TaskProjectDetailsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

extension TaskProjectDetailsViewController: UIContextMenuInteractionDelegate {
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        
        var actions = [UIAction]()
        let controllers = navigationController?.viewControllers.dropLast()
        var i = 0
        controllers?.forEach({ controller in
            let action = UIAction(title: controller.title ?? "") { action in
                self.prepareForPop()
                self.navigationController?.popToViewController(controller, animated: true)
            }
            if i == 0 {
                action.image = UIImage(systemName: "house")
            }
            actions.append(action)
            i += 1
        })

        return UIContextMenuConfiguration(identifier: nil,
          previewProvider: nil) { _ in
          UIMenu(title: "", children: actions)
        }
    }
}

extension TaskProjectDetailsViewController: PopNotifiable {
    func willPopHere() {
        self.tasksViewController.taskProjectsTableVC.reloadTable()
    }
}

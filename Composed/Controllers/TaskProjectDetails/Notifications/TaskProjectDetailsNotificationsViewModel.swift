//
//  TaskProjectDetailsNotificationsViewModel.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/15/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import Combine
import UserNotifications

final class TaskProjectDetailsNotificationsViewModel {
    
    var dataSource: [NotificationData]!
    
    class NotificationData {
        var title: String
        /// If the notification has been posted and is pending release to the user.
        @Published var notificationIsPosted: Bool = false
        /// The date the notification should trigger.
        @Published var triggerDate: Date?
        /// The text to be displayed in the second cell, or date cell, of each section.
        @Published var triggerDateDescription: String?
        var dateDescriptionPlaceholderText: String = ""
        var dateDescriptionDateInPastText: String = ""
        /// If the notification can be created depending on the conditions.
        @Published var notificationCanBePosted: Bool = false
        /// If the detail cell, or date cell, should allow user interaction.
        var detailCellShouldEnable: Bool = false
        var createNotificationClosure: (() -> ())?
        var clearNotificationClosure: (() -> ())?
        var timer: Timer?
        
        private var subscriptions = Set<AnyCancellable>()
        
        init(title: String) {
            self.title = title
            
            $triggerDate
                .sink { date in
                    self.updateTriggerDateDescription(date: date)
                    self.createAndStartTimer(date: date)
                }.store(in: &subscriptions)
        }
        
        func updateTriggerDateDescription(date: Date?) {
            if let date = date, floor(date.timeIntervalSinceNow) <= 0.0 {
                self.triggerDateDescription = self.dateDescriptionDateInPastText
            } else {
                self.triggerDateDescription = TaskProjectDetailsNotificationsViewModel.stringFromDate(date) ?? self.dateDescriptionPlaceholderText
            }
        }
        
        func createAndStartTimer(date: Date?) {
            timer?.invalidate()
            if let date = date?.zeroSeconds {
                timer = Timer(fire: date, interval: 0, repeats: false, block: { _ in
                    self.notificationCanBePosted = floor(date.timeIntervalSinceNow) > 0
                    self.updateTriggerDateDescription(date: date)
                })
                RunLoop.main.add(timer!, forMode: .common)
            }
        }
}

    private var taskProject: TaskProjectMO

    private var subscriptions = Set<AnyCancellable>()
    
    init(taskProject: TaskProjectMO) {
        self.taskProject = taskProject
        self.dataSource = [
            createCustomTimeNotificationData(),
            createDueDateNotificationData(),
            createFallingBehindNotificationData()
        ]
    }
    
    func createCustomTimeNotificationData() -> NotificationData {
        
        let notifData = NotificationData(title: "Custom time")
        notifData.dateDescriptionPlaceholderText = "Set reminder date and time."
        notifData.dateDescriptionDateInPastText = "Set reminder date and time."
        notifData.detailCellShouldEnable = true
        notifData.notificationCanBePosted = false
        notifData.createNotificationClosure = {
            if let date = notifData.triggerDate {
                self.taskProject.updateCustomTimeNotification(date: date.zeroSeconds!)
            }
        }
        notifData.clearNotificationClosure = {
            self.taskProject.clearAndPostNewLocalNotification(oldId: self.taskProject.customTimeNotificationID?.uuidString, newRequest: nil)
            self.taskProject.customTimeNotificationID = nil
        }
        
        notifData.createAndStartTimer(date: notifData.triggerDate?.zeroSeconds)
        notifData.updateTriggerDateDescription(date: nil)
        
        notifData.$triggerDate
            .sink { date in
                if let date = date {
                    notifData.notificationCanBePosted = date.zeroSeconds!.timeIntervalSinceNow > 0
                }
            }.store(in: &subscriptions)
        
        taskProject.publisher(for: \.customTimeNotificationID)
            .sink { id in
                if let id = id {
                    UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
                        let pending = requests.first(where: { $0.identifier == id.uuidString })
                        
                        if let trigger = pending?.trigger as? UNCalendarNotificationTrigger {
                            if let date = Calendar.current.date(from: trigger.dateComponents) {
                                DispatchQueue.main.async {
                                    notifData.notificationIsPosted = date.timeIntervalSinceNow > 0.0
                                    notifData.triggerDate = date.zeroSeconds
                                }
                            }
                        }
                    }
                } else {
                    notifData.notificationIsPosted = false
                }
            }.store(in: &subscriptions)
        
        return notifData
    }
    
    func createDueDateNotificationData() -> NotificationData {
        
        let notifData = NotificationData(title: "Due date")
        notifData.dateDescriptionPlaceholderText = "There's no due date."
        notifData.dateDescriptionDateInPastText = "The due date has past."
        notifData.detailCellShouldEnable = false
        notifData.createNotificationClosure = {
            if let date = notifData.triggerDate {
                self.taskProject.updateDueDateNotification(date: date)
            }
        }
        notifData.clearNotificationClosure = {
            self.taskProject.clearAndPostNewLocalNotification(oldId: self.taskProject.dueDateNotificationID?.uuidString, newRequest: nil)
            self.taskProject.dueDateNotificationID = nil
        }
        
        notifData.createAndStartTimer(date: notifData.triggerDate?.zeroSeconds)
        
        taskProject.publisher(for: \.dueDate)
            .sink { dueDate in
                notifData.triggerDate = dueDate?.zeroSeconds
                if let dueDate = dueDate?.zeroSeconds {
                    notifData.notificationCanBePosted = dueDate.timeIntervalSinceNow > 0
                } else {
                    notifData.notificationCanBePosted = false
                }
                
        }.store(in: &subscriptions)
        
        taskProject.publisher(for: \.dueDateNotificationID)
            .sink { id in
                if let id = id {
                    var pending: UNNotificationRequest?
                    UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
                        pending = requests.first(where: { $0.identifier == id.uuidString })
                        
                        if let trigger = pending?.trigger as? UNCalendarNotificationTrigger {
                            if let date = Calendar.current.date(from: trigger.dateComponents) {
                                DispatchQueue.main.async {
                                    notifData.notificationIsPosted = date.timeIntervalSinceNow > 0
                                    notifData.triggerDate = date
                                }
                            }
                        }
                    }
                } else {
                    notifData.notificationIsPosted = false
                    notifData.triggerDate = self.taskProject.dueDate
                    if let dueDate = self.taskProject.dueDate {
                        notifData.notificationCanBePosted = dueDate.timeIntervalSinceNow > 0
                    } else {
                        notifData.notificationCanBePosted = false
                    }
                }
            }.store(in: &subscriptions)
        
        return notifData
    }
    
    func createFallingBehindNotificationData() -> NotificationData {
        
        let notifData = NotificationData(title: "Falling behind")
        notifData.dateDescriptionPlaceholderText = "This is only applicable for projects with due dates."
        notifData.dateDescriptionDateInPastText = "The project is behind schedule."
        notifData.detailCellShouldEnable = false
        notifData.createNotificationClosure = {
            if let date = notifData.triggerDate {
                self.taskProject.updateFallingBehindNotification(date: date)
            }
        }
        notifData.clearNotificationClosure = {
            self.taskProject.clearAndPostNewLocalNotification(oldId: self.taskProject.fallingBehindNotificationID?.uuidString, newRequest: nil)
            self.taskProject.fallingBehindNotificationID = nil
        }
        
        notifData.createAndStartTimer(date: notifData.triggerDate)
        
        taskProject.$fallingBehindDate
            .sink { date in
                notifData.triggerDate = date
                if let date = date?.zeroSeconds {
                    notifData.notificationCanBePosted = date.timeIntervalSinceNow > 0
                } else {
                    notifData.notificationCanBePosted = false
                }
            }.store(in: &subscriptions)
        
        taskProject.publisher(for: \.fallingBehindNotificationID)
            .sink { id in
                if let id = id {
                    var pending: UNNotificationRequest?
                    UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
                        pending = requests.first(where: { $0.identifier == id.uuidString })
                        
                        if let trigger = pending?.trigger as? UNCalendarNotificationTrigger {
                            if let date = Calendar.current.date(from: trigger.dateComponents) {
                                DispatchQueue.main.async {
                                    notifData.notificationIsPosted = date.timeIntervalSinceNow > 0
                                    notifData.triggerDate = date
                                }
                            }
                        }
                    }
                } else {
                    notifData.notificationIsPosted = false
                    notifData.triggerDate = self.taskProject.fallingBehindDate
                    if let date = self.taskProject.fallingBehindDate {
                        notifData.notificationCanBePosted = date.timeIntervalSinceNow > 0
                    } else {
                        notifData.notificationCanBePosted = false
                    }
                }
            }.store(in: &subscriptions)
        
        return notifData
    }

    class func stringFromDate(_ date: Date?) -> String? {
        guard let date = date else {
            return nil
        }

        let dateFormatter = DateFormatter()
        dateFormatter.doesRelativeDateFormatting = true
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short

        return dateFormatter.string(from: date)
    }
}

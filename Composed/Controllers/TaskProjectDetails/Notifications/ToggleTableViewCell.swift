//
//  ToggleTableViewCell.swift
//  Composed
//
//  Created by Patrick Veilleux on 6/7/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine

class ToggleTableViewCell: UITableViewCell {
        
    var toggleSwitchClosure: ((Bool) -> ())?
    var toggle: UISwitch!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        toggle = UISwitch()
        toggle.addTarget(self, action: #selector(didSwitch(_:)), for: .valueChanged)
        accessoryView = toggle
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    @objc func didSwitch(_ sender: UISwitch) {
        toggleSwitchClosure?(sender.isOn)
    }
}

//
//  TaskProjectDetailsNotificationsViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 6/7/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine

class TaskProjectDetailsNotificationsViewController: UIViewController {

    private var model: TaskProjectDetailsNotificationsViewModel!
    private var taskProject: TaskProjectMO!
    
    private var tableView: UITableView!
    
    private var alertHeaderView: UIView!
    private var alertLabel: UILabel!
    var notificationsAreEnabled: Bool = false {
        didSet {
            updateUIForEnabledState(enabled: notificationsAreEnabled)
        }
    }
    
    private let kTOGGLE_CELL_ID = "ToggleCell"
    private let kDETAILS_CELL_ID = "DetailsCell"
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(model: TaskProjectDetailsNotificationsViewModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewsForDisabledState()
        setupTableView()
        updateUIForEnabledState(enabled: notificationsAreEnabled)
    }
    
    func setupTableView() {
        if tableView == nil {
            tableView = UITableView(frame: .zero, style: .grouped)
            tableView.register(ToggleTableViewCell.self, forCellReuseIdentifier: kTOGGLE_CELL_ID)
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: kDETAILS_CELL_ID)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.backgroundColor = view.backgroundColor
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 44
            view.addSubview(tableView)
            
            tableView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                tableView.topAnchor.constraint(equalTo: view.topAnchor),
                tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
            
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 64))
            let label = UILabel()
            label.font = UIFont.preferredFont(for: .title1, weight: .bold)
            label.text = "Notifications"
            headerView.addSubview(label)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 20),
                label.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -8),
                label.heightAnchor.constraint(equalToConstant: 40)
            ])
            
            tableView.tableHeaderView = headerView
        }
    }
    
    func setupViewsForDisabledState() {
        if alertHeaderView == nil {
            alertHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 64))
            let label = UILabel()
            label.font = UIFont.preferredFont(for: .title1, weight: .bold)
            label.text = "Notifications"
            alertHeaderView.addSubview(label)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                label.leadingAnchor.constraint(equalTo: alertHeaderView.leadingAnchor, constant: 20),
                label.bottomAnchor.constraint(equalTo: alertHeaderView.bottomAnchor, constant: -8),
                label.heightAnchor.constraint(equalToConstant: 40)
            ])
            
            view.addSubview(alertHeaderView)
            alertHeaderView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                alertHeaderView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                alertHeaderView.topAnchor.constraint(equalTo: view.topAnchor),
                alertHeaderView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                alertHeaderView.heightAnchor.constraint(equalToConstant: 64)
            ])
        }
        
        if alertLabel == nil {
            alertLabel = UILabel()
            alertLabel.font = UIFont.preferredFont(forTextStyle: .callout)
            alertLabel.textColor = Appearance.Color.gray
            alertLabel.numberOfLines = 0
            alertLabel.textAlignment = .center
            alertLabel.text = "Enable notifications in Settings to configure reminders."

            view.addSubview(alertLabel)
            alertLabel.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                alertLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                alertLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
                alertLabel.heightAnchor.constraint(equalToConstant: 100),
                alertLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0)
            ])
        }
    }
    
    func updateUIForEnabledState(enabled: Bool) {
        RunLoop.main.schedule {
            if enabled {
                self.tableView.isHidden = false
                self.alertHeaderView.isHidden = true
                self.alertLabel.isHidden = true
            } else {
                self.tableView.isHidden = true
                self.alertHeaderView.isHidden = false
                self.alertLabel.isHidden = false
            }
        }
    }
}

extension TaskProjectDetailsNotificationsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: kTOGGLE_CELL_ID, for: indexPath) as! ToggleTableViewCell
            cell.textLabel?.text = model.dataSource[indexPath.section].title
            
            model.dataSource[indexPath.section].$notificationIsPosted
                .sink { posted in
                    RunLoop.main.schedule {
                        cell.toggle.isOn = posted
                    }
                }.store(in: &subscriptions)
            
            model.dataSource[indexPath.section].$notificationCanBePosted
                .sink { canBePosted in
                    RunLoop.main.schedule {
                        cell.toggle.isEnabled = canBePosted
                        cell.textLabel?.textColor = canBePosted ? .label : Appearance.Color.gray
                    }
                }.store(in: &subscriptions)
            
            cell.backgroundColor = view.backgroundColor
            cell.selectionStyle = .none

            cell.toggleSwitchClosure = { isOn in
                if isOn {
                    self.model.dataSource[indexPath.section].createNotificationClosure?()
                } else {
                    self.model.dataSource[indexPath.section].clearNotificationClosure?()
                }
            }

            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: kDETAILS_CELL_ID, for: indexPath)
            
            model.dataSource[indexPath.section].$notificationCanBePosted
                .sink { canBePosted in
                    RunLoop.main.schedule {
                        let active = self.model.dataSource[indexPath.section].detailCellShouldEnable
                        cell.textLabel?.textColor = active ? .label : Appearance.Color.gray
                        cell.isUserInteractionEnabled = active
                    }
                }.store(in: &subscriptions)
            
            cell.backgroundColor = view.backgroundColor
//            cell.textLabel?.textColor = model.dataSource[indexPath.section].detailCellShouldEnable ? .label : Appearance.Color.gray
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            cell.textLabel?.minimumScaleFactor = 0.5
            
            model.dataSource[indexPath.section].$triggerDateDescription
                .sink { text in
                    RunLoop.main.schedule {
                        cell.textLabel?.text = text
                    }
                }.store(in: &subscriptions)

            return cell
        }
    }
}

extension TaskProjectDetailsNotificationsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            // Present date picker
            let datePicker = DatePickerViewController(title: "Select Date", message: nil, preferredStyle: .actionSheet)
            let triggerDate = model.dataSource[indexPath.section].triggerDate ?? Date().addingTimeInterval(60)
            datePicker.datePicker.date = triggerDate
            datePicker.datePicker.minimumDate = Date().addingTimeInterval(60)
            
            let done = UIAlertAction(title: "Done", style: .cancel) { (action) in
                self.tableView.deselectRow(at: indexPath, animated: true)
                self.model.dataSource[indexPath.section].triggerDate = datePicker.datePicker.date
                if self.model.dataSource[indexPath.section].notificationIsPosted {
                    self.model.dataSource[indexPath.section].createNotificationClosure?()
                }
            }
            datePicker.addAction(done)
            
            present(datePicker, animated: true)
        }
    }
}

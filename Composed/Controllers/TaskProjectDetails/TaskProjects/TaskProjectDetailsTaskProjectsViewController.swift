//
//  TaskProjectDetailsTaskProjectsViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 5/17/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine
import CoreData

class TaskProjectDetailsTaskProjectsViewController: TaskProjectsViewController {

    private var parentTaskProject: TaskProjectMO!
    
    private var noTasksLabel: UILabel!
    private var noTasksLabelAnimationDuration: Double = 0
    
    var publisher = PassthroughSubject<Action, Never>()
    
    enum Action {
        case didSelectTaskProject(TaskProjectMO)
        case addNewTaskProject
    }
    
    override var taskRequest: NSFetchRequest<TaskProjectMO> {
        let sortDescriptor = NSSortDescriptor(key: #keyPath(TaskProjectMO.title), ascending: true)
        let request: NSFetchRequest<TaskProjectMO> = TaskProjectMO.fetchRequest()
        request.predicate = NSPredicate(format: "parent.id == %@ && kind == 0", parentTaskProject.id as CVarArg)
        request.sortDescriptors = [sortDescriptor]
        return request
    }
    
    override var projectRequest: NSFetchRequest<TaskProjectMO> {
        let sortDescriptor = NSSortDescriptor(key: #keyPath(TaskProjectMO.title), ascending: true)
        let request: NSFetchRequest<TaskProjectMO> = TaskProjectMO.fetchRequest()
        request.predicate = NSPredicate(format: "parent.id == %@ && kind == 1", parentTaskProject.id as CVarArg)
        request.sortDescriptors = [sortDescriptor]
        return request
    }
    
    init(parentTaskProject: TaskProjectMO) {
        self.parentTaskProject = parentTaskProject
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNoTasksLabel()
        
        taskProjectsTableVC.tableView.scrollIndicatorInsets = UIEdgeInsets(top: 20, left: 0, bottom: 8, right: 0)
        
        taskProjectsTableVC.projectSortMethod = parentTaskProject.projectSortMethod
        taskProjectsTableVC.taskSortMethod = parentTaskProject.taskSortMethod
        
        taskProjectsTableVC.$projectSortMethod
            .sink { [unowned self] sortMethod in
                self.parentTaskProject.projectSortMethod = sortMethod
            }.store(in: &subscriptions)
        
        taskProjectsTableVC.$taskSortMethod
            .sink { [unowned self] sortMethod in
                self.parentTaskProject.taskSortMethod = sortMethod
            }.store(in: &subscriptions)
    }
    
    private func setupNoTasksLabel() {
        if noTasksLabel == nil {
            noTasksLabel = UILabel()
            noTasksLabel.font = UIFont.preferredFont(forTextStyle: .callout)
            noTasksLabel.textColor = Appearance.Color.gray
            noTasksLabel.numberOfLines = 0
            noTasksLabel.textAlignment = .center
            
            parentTaskProject.publisher(for: \.title)
                .sink { title in
                    RunLoop.main.schedule {
                        self.noTasksLabel.text = "Add tasks to convert \(title) into a project."
                    }
                }.store(in: &subscriptions)
            
            parentTaskProject.publisher(for: \.children)
                .sink { children in
                    RunLoop.main.schedule {
                        UIView.animate(withDuration: self.noTasksLabelAnimationDuration, animations: {
                            if children.count == 0 {
                                self.noTasksLabel.alpha = 1
                                self.taskProjectsTableVC.tableView.alpha = 0
                            } else {
                                self.noTasksLabel.alpha = 0
                                self.taskProjectsTableVC.tableView.alpha = 1
                            }
                        }) { _ in
                            self.noTasksLabelAnimationDuration = 0.3
                        }
                    }
                }.store(in: &subscriptions)

            view.addSubview(noTasksLabel)
            noTasksLabel.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                noTasksLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                noTasksLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
                noTasksLabel.heightAnchor.constraint(equalToConstant: 100),
                noTasksLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0)
            ])
        }
    }
    
    override func didSelectTaskProject(_ taskProject: TaskProjectMO) {
        publisher.send(.didSelectTaskProject(taskProject))
    }
    
    // MARK: - ADD BUTTON
    
    override func didTapFloatingActionButton(_ sender: Any) {
        publisher.send(.addNewTaskProject)
    }
}

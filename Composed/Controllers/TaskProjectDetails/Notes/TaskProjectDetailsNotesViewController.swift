//
//  TaskProjectDetailsNotesViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 5/17/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine

class TaskProjectDetailsNotesViewModel {
    
    @Published var notes: String
    
    init(notes: String) {
        self.notes = notes
    }
}

class TaskProjectDetailsNotesViewController: UIViewController {

    var model: TaskProjectDetailsNotesViewModel!
    
    private var headerView: UIView!
    private var textView: EditableDataTextView!
    private var doneButton: UIButton!
    private var doneButtonBottomConstraint: NSLayoutConstraint!
    private var doneButtonBottomSpacing: CGFloat = 8
    
    private var noNotesLabel: UILabel!
    
    private var subscriptions: [AnyCancellable] = []
    
    init(model: TaskProjectDetailsNotesViewModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTextView()
        setupHeaderView()
        setupNoNotesLabel()
        configureUIForModel()
    }
    
    private func setupHeaderView() {
        if headerView == nil {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 64))
            let label = UILabel()
            label.font = UIFont.preferredFont(for: .title1, weight: .bold)
            label.text = "Notes"
            headerView.addSubview(label)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 20),
                label.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -8),
                label.heightAnchor.constraint(equalToConstant: 40)
            ])
            
            textView.addSubview(headerView)
            headerView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                headerView.leadingAnchor.constraint(equalTo: textView.leadingAnchor),
                headerView.topAnchor.constraint(equalTo: textView.topAnchor),
                headerView.trailingAnchor.constraint(equalTo: textView.trailingAnchor),
                headerView.heightAnchor.constraint(equalToConstant: 64)
            ])
        }
    }
    
    // MARK: - Text View
    
    private func setupTextView() {
        if textView == nil {
            textView = EditableDataTextView()
            textView.font = UIFont.preferredFont(forTextStyle: .body)
            textView.textContainerInset = UIEdgeInsets(top: 64, left: 16, bottom: 20, right: 16)
            textView.dataDetectorTypes = .all
            textView.backgroundColor = .clear
            textView.verticalScrollIndicatorInsets = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
            
            textView.linkTextAttributes = [
                .foregroundColor: Appearance.Color.blue,
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .underlineColor: UIColor.clear
            ]
            
            // Update the model with changes to the textView
            NotificationCenter.default
                .publisher(for: UITextView.textDidChangeNotification, object: textView)
                .debounce(for: .milliseconds(500), scheduler: RunLoop.main)
                .map { ($0.object as! UITextView).text }
                .removeDuplicates()
                .sink { (text) in
                    self.model.notes = text ?? ""
            }.store(in: &subscriptions)
            
            view.addSubview(textView)
            textView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                textView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                textView.topAnchor.constraint(equalTo: view.topAnchor),
                textView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ])
            
            let doneButton = UIButton()
            doneButton.setTitle("Done", for: .normal)
            doneButton.backgroundColor = Appearance.Color.blue
            doneButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
            doneButton.addTarget(self, action: #selector(didTapDoneButton), for: .touchUpInside)
            doneButton.layer.cornerRadius = 20
            doneButton.layer.masksToBounds = true

            let tb = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 56))
            tb.addSubview(doneButton)

            doneButton.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                doneButton.leadingAnchor.constraint(equalTo: tb.safeAreaLayoutGuide.leadingAnchor, constant: 8),
                doneButton.heightAnchor.constraint(equalToConstant: 40),
                doneButton.trailingAnchor.constraint(equalTo: tb.safeAreaLayoutGuide.trailingAnchor, constant: -8),
                doneButton.centerYAnchor.constraint(equalTo: tb.centerYAnchor)
            ])

            textView.inputAccessoryView = tb
            
            NotificationCenter.default
                .publisher(for: UIResponder.keyboardWillShowNotification)
                .sink { (notification) in
                    self.keyboardWillShow(notification: notification)
            }.store(in: &subscriptions)

            NotificationCenter.default
                .publisher(for: UIResponder.keyboardWillHideNotification)
                .sink { (notification) in
                    self.keyboardWillHide(notification: notification)
            }.store(in: &subscriptions)
        }
    }
    
    private func setupNoNotesLabel() {
        if noNotesLabel == nil {
            noNotesLabel = UILabel()
            noNotesLabel.font = UIFont.preferredFont(forTextStyle: .callout)
            noNotesLabel.textColor = Appearance.Color.gray
            noNotesLabel.numberOfLines = 0
            noNotesLabel.textAlignment = .center
            noNotesLabel.text = "Tap here to add notes."
            
            model.$notes.sink { (notes) in
                if notes.isEmpty {
                    self.noNotesLabel.alpha = 1
                } else {
                    self.noNotesLabel.alpha = 0
                }
            }.store(in: &subscriptions)

            view.addSubview(noNotesLabel)
            noNotesLabel.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                noNotesLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                noNotesLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
                noNotesLabel.heightAnchor.constraint(equalToConstant: 100),
                noNotesLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0)
            ])
        }
    }
    
    private func configureUIForModel() {
        textView.text = model.notes
    }
    
    @objc private func didTapDoneButton() {
        _ = textView.resignFirstResponder()
        textView.isEditable = false
    }
    
    //MARK: - Keyboard
    
    private func keyboardWillShow(notification: Notification) {
        textView.layoutIfNeeded() // Fixes snapshotting a view that has not been rendered error

        // Adjust the textView contentInset when the keyboard is shown to prevent it from being hidden behind the keyboard
        let userInfo = notification.userInfo
        let keyboardFrame = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let spacing = keyboardFrame.height
        self.textView.contentInset.bottom = spacing
        self.textView.verticalScrollIndicatorInsets.bottom = spacing
        textView.scrollRangeToVisible(textView.selectedRange)
        
        // Some dumb code that fixes an issue with the text view not being scrollable when the keyboard appears - iOS 13.5.1
        // Issue persists when using standard UITextView also, not just EditableDataTextView
        let temp = textView.text + " "
        textView.text = temp
        textView.text = String(temp.dropLast())
    }

    private func keyboardWillHide(notification: Notification) {
        // Adjust the textView contentInset when the keyboard is hidden to restore the normal view
        self.textView.contentInset.bottom = 0
        self.textView.verticalScrollIndicatorInsets.bottom = 20
    }
}

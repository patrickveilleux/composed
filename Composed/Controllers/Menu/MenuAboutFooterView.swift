//
//  MenuAboutFooterView.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/22/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

class MenuAboutFooterView: UIView {

    var logoView: UIImageView!
    
    init() {
        super.init(frame: .zero)
        
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        logoView.layer.cornerRadius = logoView.bounds.width / 5
        logoView.layer.masksToBounds = true
    }
    
    func setupView() {
        logoView = UIImageView(image: UIApplication.shared.icon)
        logoView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(logoView)
        
        logoView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        logoView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        logoView.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        logoView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        let nameLabel = UILabel()
        nameLabel.text = "Composed"
        nameLabel.font = .preferredFont(for: .subheadline, weight: .bold)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(nameLabel)
        
        nameLabel.topAnchor.constraint(equalTo: logoView.bottomAnchor, constant: 8).isActive = true
        nameLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        let versionLabel = UILabel()
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
            let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLabel.text = version + " (\(build))"
        }
        
        versionLabel.font = .preferredFont(for: .subheadline, weight: .bold)
        versionLabel.textColor = Appearance.Color.gray
        versionLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(versionLabel)
        
        versionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4).isActive = true
        versionLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        let developerLabel = UILabel()
        developerLabel.font = .preferredFont(for: .subheadline, weight: .bold)
        developerLabel.numberOfLines = 0
        developerLabel.textAlignment = .center
        let base = "Designed and developed by "
        let name = "Patrick Veilleux."
        let baseAttrs = [NSAttributedString.Key.foregroundColor: Appearance.Color.gray]
        let nameAttrs = [NSAttributedString.Key.foregroundColor: UIColor.label]
        let attrBase = NSMutableAttributedString(string: base, attributes: baseAttrs)
        let attrName = NSAttributedString(string: name, attributes: nameAttrs)
        attrBase.append(attrName)
        developerLabel.attributedText = attrBase
        
        developerLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(developerLabel)
        
        developerLabel.topAnchor.constraint(equalTo: versionLabel.bottomAnchor, constant: 16).isActive = true
        developerLabel.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 20).isActive = true
        developerLabel.trailingAnchor.constraint(greaterThanOrEqualTo: trailingAnchor, constant: -20).isActive = true
        developerLabel.bottomAnchor.constraint(greaterThanOrEqualTo: bottomAnchor, constant: -20).isActive = true
        developerLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        heightAnchor.constraint(greaterThanOrEqualToConstant: 240).isActive = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}

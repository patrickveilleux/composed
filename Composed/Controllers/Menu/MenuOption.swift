//
//  MenuOption.swift
//  Composed
//
//  Created by Patrick Veilleux on 5/8/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import UIKit

enum MenuSection: CaseIterable {
    case settings
    case contact
    case support
    
    var title: String {
        switch self {
        case .settings:     return "Settings"
        case .contact:      return "Reach out"
        case .support:      return "Help"
        }
    }
    
    var items: [MenuOption] {
        switch self {
        case .settings:
            return [
                SwitchableMenuItem(title: "Dark App Icon",
                                   image: UIImage(systemName: "square.fill")!,
                                   isOn: UIApplication.shared.alternateIconName == "Logo_Dark",
                                   action: { isOn in
                                        if isOn {
                                            UIApplication.shared.setAlternateIconName("Logo_Dark")
                                        } else {
                                            UIApplication.shared.setAlternateIconName(nil)
                                        }
                })
            ]
        case .contact:
            return [
                EmailMenuItem(title: "Email",
                              image: UIImage(systemName: "envelope.fill")!,
                              address: "composedsoftware@gmail.com"),
                LinkableMenuItem(title: "Twitter",
                                 image: UIImage(systemName: "text.bubble.fill")!,
                                 url: URL(string: "https://www.twitter.com/composedpm")!),
                LinkableMenuItem(title: "Website",
                                 image: UIImage(systemName: "globe")!,
                                 url: URL(string: "https://www.composedpm.com")!)
            ]
        case .support:
            return [
                ModalDetailMenuItem(title: "Help",
                                    image: UIImage(systemName: "questionmark.diamond.fill")!,
                                    controller: GuideViewController()),
                LinkableMenuItem(title: "Privacy Policy",
                                 image: UIImage(systemName: "hand.raised.fill")!,
                                 url: URL(string: "https://www.composedpm.com/privacy-policy")!)
            ]
        }
    }
}

protocol MenuOption {
    var title: String { get }
    var image: UIImage { get }
    var accessoryType: UITableViewCell.AccessoryType { get }
}

extension MenuOption {
    var accessoryType: UITableViewCell.AccessoryType { return .none }
}

protocol EmailMenuOption: MenuOption {
    var address: String { get }
}

struct EmailMenuItem: EmailMenuOption {
    let title: String
    let image: UIImage
    let address: String
}

protocol LinkableMenuOption: MenuOption {
    var url: URL { get }
}

struct LinkableMenuItem: LinkableMenuOption {
    let title: String
    let image: UIImage
    let url: URL
}

protocol SwitchableMenuOption: MenuOption {
    var isOn: Bool { get set }
    var action: (Bool) -> () { get }
}

class SwitchableMenuItem: SwitchableMenuOption {
    let title: String
    let image: UIImage
    var isOn: Bool {
        didSet {
            action(isOn)
        }
    }
    let action: (Bool) -> ()
    
    init(title: String, image: UIImage, isOn: Bool, action: @escaping (Bool) -> ()) {
        self.title = title
        self.image = image
        self.isOn = isOn
        self.action = action
    }
}

protocol ModalDetailMenuOption: MenuOption {
    var controller: UIViewController { get }
}

struct ModalDetailMenuItem: ModalDetailMenuOption {
    let title: String
    let image: UIImage
    let controller: UIViewController
}

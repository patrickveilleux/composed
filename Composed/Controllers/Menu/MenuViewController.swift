//
//  MenuViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 5/8/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import MessageUI

class MenuViewController: UIViewController {
        
    var tableView: UITableView!
    
    let kTOGGLE_CELL_ID = "ToggleCellID"
    let kSTANDARD_CELL_ID = "StandardCellID"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        title = "Menu"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        setupTableView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.tableFooterView = MenuAboutFooterView()
        
        guard let footerView = self.tableView.tableFooterView else { return }
        
        let width = self.tableView.bounds.size.width
        let size = footerView.systemLayoutSizeFitting(CGSize(width: width, height:  UIView.layoutFittingCompressedSize.height))
        
        if footerView.frame.size.height != size.height {
            footerView.frame.size.height = size.height
            self.tableView.tableFooterView = footerView
        }
    }
    
    func setupTableView() {
        if tableView == nil {
            tableView = UITableView(frame: .zero, style: .grouped)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.register(ToggleTableViewCell.self, forCellReuseIdentifier: kTOGGLE_CELL_ID)
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: kSTANDARD_CELL_ID)
            
            tableView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(tableView)
            
            NSLayoutConstraint.activate([
                tableView.topAnchor.constraint(equalTo: view.topAnchor),
                tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
        }
    }
    
    func composeMail(to recipient: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["composedsoftware@gmail.com"])
            mail.setSubject("Composed Feedback")
            present(mail, animated: true)
        } else {
            let alert = UIAlertController(title: "Unable to Send Email", message: "This device does not have an email account setup.", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Okay", style: .default)
            alert.addAction(cancel)
            present(alert, animated: true)
        }
    }
    
    func openURL(_ url: URL) {
        UIApplication.shared.open(url)
    }
}

extension MenuViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return MenuSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuSection.allCases[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = MenuSection.allCases[indexPath.section].items[indexPath.row]
        if var item = item as? SwitchableMenuOption,
            let cell = tableView.dequeueReusableCell(withIdentifier: kTOGGLE_CELL_ID,
                                                     for: indexPath) as? ToggleTableViewCell {
            cell.textLabel?.text = item.title
            cell.imageView?.image = item.image
            cell.imageView?.tintColor = .label
            cell.accessoryType = item.accessoryType
            cell.toggle.isOn = item.isOn
            cell.toggleSwitchClosure = { isOn in
                item.isOn = isOn
            }
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: kSTANDARD_CELL_ID,
                                                     for: indexPath) as UITableViewCell
            cell.textLabel?.text = item.title
            cell.imageView?.image = item.image
            cell.imageView?.tintColor = .label
            cell.accessoryType = item.accessoryType
            cell.selectionStyle = .default
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return MenuSection.allCases[section].title
    }
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = MenuSection.allCases[indexPath.section].items[indexPath.row]
        if let _ = item as? SwitchableMenuOption {
            return
        } else if let item = item as? EmailMenuOption {
            composeMail(to: item.address)
        } else if let item = item as? LinkableMenuOption {
            openURL(item.url)
        } else if let item = item as? ModalDetailMenuOption {
            present(item.controller, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MenuViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

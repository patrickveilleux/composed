//
//  TaskProjectsTableViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 4/15/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine
import CoreData

class TaskProjectsTableViewController: UITableViewController {

    private var persistentContainer: NSPersistentContainer!
    private var taskRequest: NSFetchRequest<TaskProjectMO>!
    private var projectRequest: NSFetchRequest<TaskProjectMO>!
    
    @Published var projectSortMethod: TaskProjectSortMethod.Project = .onScheduleBackwards
    @Published var taskSortMethod: TaskProjectSortMethod.Task = .completedTrueLast
    
    typealias DataSource = [[TaskProjectMO]]
    private var dataSource = DataSource() {
        didSet {
            if dataSource.count == 2 {
                numberOfTaskProjects = dataSource[0].count + dataSource[1].count
            }
        }
    }
    @Published var numberOfTaskProjects = 0
    
    private let kPROJECT_CELL_ID = "ProjectCell"
    private let kTASK_CELL_ID = "TaskCell"
    private let kHEADER_VIEW_ID = "HeaderView"
    
    var backgroundColor: UIColor?
            
    var subscriptions = Set<AnyCancellable>()
    var publisher = PassthroughSubject<Action, Never>()
                
    enum Action {
        case didSelectTaskProject(TaskProjectMO)
    }
        
    init(taskRequest: NSFetchRequest<TaskProjectMO>, projectRequest: NSFetchRequest<TaskProjectMO>) {
        self.taskRequest = taskRequest
        self.projectRequest = projectRequest
        
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        persistentContainer = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        view.backgroundColor = backgroundColor
        
        tableView.register(UINib(nibName: "TaskTableViewCell", bundle: nil), forCellReuseIdentifier: kTASK_CELL_ID)
        tableView.register(UINib(nibName: "ProjectTableViewCell", bundle: nil), forCellReuseIdentifier: kPROJECT_CELL_ID)
        tableView.register(TaskProjectTableHeaderView.self, forHeaderFooterViewReuseIdentifier: kHEADER_VIEW_ID)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 52
        
        setupSortMethodSubscriptions()
        
        fetchData(projectSort: projectSortMethod, taskSort: taskSortMethod)
    }
    
    //MARK: - DataSource
    
    private func fetchData(projectSort: TaskProjectSortMethod.Project, taskSort: TaskProjectSortMethod.Task) {
        do {
            let projectResults = try persistentContainer.viewContext.fetch(projectRequest)
            let sortedProjects = projectSortMethod.sort(items: projectResults)
            
            let taskResults = try persistentContainer.viewContext.fetch(taskRequest)
            let sortedTasks = taskSortMethod.sort(items: taskResults)
            
            dataSource = [sortedProjects, sortedTasks]
        } catch {
            print(error)
        }
    }
    
    private func sortData() {
        let projects = projectSortMethod.sort(items: dataSource[0])
        let tasks = taskSortMethod.sort(items: dataSource[1])
        dataSource = [projects, tasks]
    }
    
    func reloadTable() {
        fetchData(projectSort: projectSortMethod, taskSort: taskSortMethod)
        tableView.reloadData()
    }
    
    private func setupSortMethodSubscriptions() {
        $projectSortMethod
            .dropFirst()
            .sink { method in
                RunLoop.main.schedule {
                    self.fetchData(projectSort: method, taskSort: self.taskSortMethod)
                    self.tableView.reloadData()
                }
            }.store(in: &subscriptions)
        
        $taskSortMethod
            .dropFirst()
            .sink { method in
                RunLoop.main.schedule {
                    self.fetchData(projectSort: self.projectSortMethod, taskSort: method)
                    self.tableView.reloadData()
                }
            }.store(in: &subscriptions)
    }
    
    // MARK: - TableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: self.kPROJECT_CELL_ID, for: indexPath) as! ProjectTableViewCell

            let item = dataSource[indexPath.section][indexPath.row]
            cell.taskProject = item
            cell.configure()
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: self.kTASK_CELL_ID, for: indexPath) as! TaskTableViewCell
            
            let item = dataSource[indexPath.section][indexPath.row]
            cell.taskProject = item
            cell.configure()
            cell.taskCompletionUpdateClosure = { [unowned self] _ in
                switch self.taskSortMethod {
                case .completedTrueLast, .dueDateForwards:
                    RunLoop.main.schedule {
                        self.sortData()
                        self.reloadTable()
                    }
                default:
                    break
                }
            }
            
            return cell
        }
    }
    
    // MARK: - TableViewDelegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = self.view.backgroundColor
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: kHEADER_VIEW_ID) as! TaskProjectTableHeaderView
        view.contentView.backgroundColor = self.view.backgroundColor
        view.title = section == 0 ? "Projects" : "Tasks"
        view.sortButtonClosure = {
            let section = section == 0 ? TaskProjectMO.Kind.project : TaskProjectMO.Kind.task
            self.presentSortViewController(section: section)
        }
        return view
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let taskProject = dataSource[indexPath.section][indexPath.row]
        publisher.send(.didSelectTaskProject(taskProject))
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (contextualAction, view, completionHandler) in
            
            let item = self.dataSource[indexPath.section][indexPath.row]
            
            // Confirm deletion with user
            let alert = UIAlertController(title: "Are you sure you want to delete \(item.title)?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
                self.delete(taskProject: item, at: indexPath)
                completionHandler(true)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                completionHandler(true)
            }))
            
            self.present(alert, animated: true)
        }
        deleteAction.image = UIImage(systemName: "trash.fill")
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    private func bringSectionHeadersToFront() {
        if let header = self.tableView.headerView(forSection: 0) {
            self.tableView.bringSubviewToFront(header)
        }
        if let header = self.tableView.headerView(forSection: 1) {
            self.tableView.bringSubviewToFront(header)
        }
    }
    
    private func delete(taskProject: TaskProjectMO, at indexPath: IndexPath) {
        self.persistentContainer.viewContext.delete(taskProject)
        
        self.dataSource[indexPath.section].remove(at: indexPath.row)
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
    
    // MARK: - Sort View
    
    private func presentSortViewController(section: TaskProjectMO.Kind) {        
        var methods: [SortMethod]
        switch section {
        case .project:
            methods = TaskProjectSortMethod.Project.allCases
        case .task:
            methods = TaskProjectSortMethod.Task.allCases
        }
        
        let activeMethod: SortMethod = section == .project ? projectSortMethod : taskSortMethod
        
        let sortVC = SortViewController(methods: methods, activeMethod: activeMethod)
        sortVC.publisher.sink { action in
            switch action {
            case .didEndWithSortMethod(let sortMethod):
                if let sortMethod = sortMethod {
                    switch section {
                    case .project:
                        self.projectSortMethod = sortMethod as! TaskProjectSortMethod.Project
                    case .task:
                        self.taskSortMethod = sortMethod as! TaskProjectSortMethod.Task
                    }
                }
                self.dismiss(animated: true)
            }
        }.store(in: &subscriptions)
        
        sortVC.modalPresentationStyle = .overFullScreen
        sortVC.modalTransitionStyle = .crossDissolve
        present(sortVC, animated: true)
    }
}

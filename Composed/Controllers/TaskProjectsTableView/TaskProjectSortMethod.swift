//
//  TaskProjectSortMethod.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/15/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import UIKit

protocol SortMethod {
    var string: String { get }
    var image: UIImage { get }
    func sort(items: [TaskProjectMO]) -> [TaskProjectMO]
}

public enum TaskProjectSortMethod {
    case tasks
    case projects
    
    @objc public enum Task: Int16, CaseIterable, SortMethod {
        case titleAZ
        case completedTrueLast
        case createdDateForwards
        case dueDateForwards
        
        var string: String {
            switch self {
            case .titleAZ:                  return "Title"
            case .createdDateForwards:      return "Created date"
            case .dueDateForwards:          return "Due date"
            case .completedTrueLast:        return "Completed"
            }
        }
        
        var image: UIImage {
            switch self {
            case .titleAZ:                  return UIImage(systemName: "textformat.abc")!
            case .createdDateForwards:      return UIImage(systemName: "pencil")!
            case .dueDateForwards:          return UIImage(systemName: "calendar")!
            case .completedTrueLast:        return UIImage(systemName: "checkmark")!
            }
        }
        
        func sort(items: [TaskProjectMO]) -> [TaskProjectMO] {
            switch self {
            case .titleAZ:
                return items.sorted(by: { $0.title < $1.title })
            case .createdDateForwards:
                return items.sorted(by: { $0.title < $1.title })
                    .sorted(by: { $0.dateCreated < $1.dateCreated })
            case .dueDateForwards:
                return items.sorted(by: { $0.title < $1.title })
                    .sorted(by: { $0.isComplete > $1.isComplete })
                    .sorted(by: { $0.dueDate < $1.dueDate })
            case .completedTrueLast:
                return items.sorted(by: { $0.dateCreated < $1.dateCreated })
                    .sorted(by: { $0.isComplete < $1.isComplete })
            }
        }
    }
    
    @objc public enum Project: Int16, CaseIterable, SortMethod {
        case titleAZ
        case onScheduleBackwards
        case createdDateForwards
        case dueDateForwards
        
        var string: String {
            switch self {
            case .titleAZ:                  return "Title"
            case .createdDateForwards:      return "Created date"
            case .dueDateForwards:          return "Due date"
            case .onScheduleBackwards:      return "On schedule"
            }
        }
        
        var image: UIImage {
            switch self {
            case .titleAZ:                  return UIImage(systemName: "textformat.abc")!
            case .createdDateForwards:      return UIImage(systemName: "pencil")!
            case .dueDateForwards:          return UIImage(systemName: "calendar")!
            case .onScheduleBackwards:      return UIImage(systemName: "chevron.right.2")!
            }
        }
        
        func sort(items: [TaskProjectMO]) -> [TaskProjectMO] {
            switch self {
            case .titleAZ:
                return items.sorted(by: { $0.title < $1.title })
            case .createdDateForwards:
                return items.sorted(by: { $0.title < $1.title })
                    .sorted(by: { $0.dateCreated < $1.dateCreated })
            case .dueDateForwards:
                return items.sorted(by: { $0.title < $1.title })
                    .sorted(by: { $0.isComplete < $1.isComplete })
                    .sorted(by: { $0.dueDate < $1.dueDate })
            case .onScheduleBackwards:
                return items.sorted(by: { $0.progressStatusPercentage < $1.progressStatusPercentage })
                    .sorted(by: { $0.isComplete < $1.isComplete })
            }
        }
    }
}

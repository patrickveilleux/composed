//
//  TaskProjectMO+CoreDataProperties.swift
//  Composed
//
//  Created by Patrick Veilleux on 6/24/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//
//

import Foundation
import CoreData
import UserNotifications

extension TaskProjectMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TaskProjectMO> {
        return NSFetchRequest<TaskProjectMO>(entityName: "TaskProject")
    }
    
    public class var entityName: String {
        return "TaskProject"
    }

    @NSManaged public var dateCreated: Date
    @NSManaged public var dueDate: Date?
    @NSManaged public var id: UUID
    @NSManaged public var kind: Kind
    @NSManaged public var notes: String
    @NSManaged public var title: String
    @NSManaged public var taskComplete: Bool
    @NSManaged public var children: Set<TaskProjectMO>
    @NSManaged public var parent: TaskProjectMO?
    @NSManaged public var customTimeNotificationID: UUID?
    @NSManaged public var dueDateNotificationID: UUID?
    @NSManaged public var fallingBehindNotificationID: UUID?
    @NSManaged public var taskSortMethod: TaskProjectSortMethod.Task
    @NSManaged public var projectSortMethod: TaskProjectSortMethod.Project
    
}

// MARK: Generated accessors for children
extension TaskProjectMO {

    @objc(addChildrenObject:)
    @NSManaged public func addToChildren(_ value: TaskProjectMO)

    @objc(removeChildrenObject:)
    @NSManaged public func removeFromChildren(_ value: TaskProjectMO)

    @objc(addChildren:)
    @NSManaged public func addToChildren(_ values: NSSet)

    @objc(removeChildren:)
    @NSManaged public func removeFromChildren(_ values: NSSet)

}

// MARK: - Computed properties
extension TaskProjectMO {
    
    var isComplete: Bool {
        return self.kind == .project ? taskProgress == 1.0 : taskComplete
    }
    
    @objc public enum Kind: Int16, CaseIterable {
        case task       = 0
        case project    = 1
        
        var string: String {
            switch self {
            case .task:
                return "task"
            case .project:
                return "project"
            }
        }
    }
    
    enum ProgressStatus {
        case complete
        case ahead
        case warning
        case behind
        case pastDue
    }
    
    var progressStatus: ProgressStatus {
        if taskProgress == 1.0 {
            return .complete
        }
        
        guard let timeProgress = timeProgress else {
            return .ahead
        }
        
        let nextTaskProgress = Float(numberOfCompleteSubtasks() + 1) / Float(numberOfSubtasks())
        if timeProgress < 0 || (timeProgress >= 1.0 && !isComplete) {
            return .pastDue
        } else if taskProgress >= timeProgress {
            return .ahead
        } else if nextTaskProgress >= timeProgress {
            return .warning
        } else {
            return .behind
        }
    }
    
    var progressStatusPercentage: Float {
        return taskProgress - (timeProgress ?? 0)
    }
    
    var taskProgress: Float {
        return Float(numberOfCompleteSubtasks()) / Float(numberOfSubtasks())
    }
    
    var timeProgress: Float? {
        if let dueDate = self.dueDate {
            let elapsedTime = Date().timeIntervalSince(dateCreated)
            let totalTime = dueDate.timeIntervalSince(dateCreated)
            return Float(elapsedTime / totalTime).clamped(to: 0...1)
        }
        return nil
    }
    
    func updateFallingBehindDate(propagate: Bool, recursive: Bool) {
        if propagate {
            if recursive {
                parent?.updateFallingBehindDate(propagate: true, recursive: true)
            } else {
                parent?.updateFallingBehindDate(propagate: true, recursive: true)
            }
        }
        
        guard let dueDate = self.dueDate, kind == .project else {
            self.fallingBehindDate = nil
            return
        }
        
        let totalTime = dueDate.timeIntervalSince(dateCreated)
        let fallingBehindInterval = totalTime * Double(taskProgress)
        self.fallingBehindDate = Date(timeInterval: fallingBehindInterval, since: dateCreated)
    }
    
    func numberOfSubtasks() -> Int {
        var count = 0
        for case let item in children where item.kind == .task {
            count += 1
        }
        for case let item in children where item.kind == .project {
            count += item.numberOfSubtasks()
        }
        return count
    }
    
    func numberOfCompleteSubtasks() -> Int {
        var count = 0
        for case let item in children where item.kind == .task && item.isComplete {
            count += 1
        }
        for case let item in children where item.kind == .project {
            count += item.numberOfCompleteSubtasks()
        }
        return count
    }
    
    var rootTaskProject: TaskProjectMO? {
        return parent?.rootTaskProject ?? self
    }
    
    // MARK: - NOTIFICATIONS
    
    enum NotificationType: Int {
        case customTime
        case dueDate
        case fallingBehind
        
        var string: String {
            switch self {
            case .customTime:       return "Custom time"
            case .dueDate:          return "Due date"
            case .fallingBehind:    return "Falling behind"
            }
        }
    }
    
    private func createCustomTimeNotification(date: Date) -> UNNotificationRequest? {
        guard date.timeIntervalSinceNow > 0 else { return nil }
        
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "Reminder about your \(kind.string): \(title)"
        content.sound = UNNotificationSound.default
        
        if kind == .project {
            let remainingSubtasks = numberOfSubtasks() - numberOfCompleteSubtasks()
            content.body = "There are \(remainingSubtasks) tasks remaining."
        }
        
        let userInfo = ["TaskProjectID": id.uuidString, "Type": NotificationType.customTime.rawValue] as [String : Any]
        content.userInfo = userInfo
        
        let id = UUID().uuidString
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        
        return request
    }
    
    private func createDueDateNotification(dueDate: Date?) -> UNNotificationRequest? {
        guard let date = dueDate, date.timeIntervalSinceNow > 0 else { return nil }
        
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "The \(kind.string) \"\(self.title)\" is due!"
        content.sound = UNNotificationSound.default
        
        if kind == .project {
            let remainingSubtasks = numberOfSubtasks() - numberOfCompleteSubtasks()
            content.body = "There are \(remainingSubtasks) tasks remaining."
        }
        
        let userInfo = ["TaskProjectID": id.uuidString, "Type": NotificationType.dueDate.rawValue] as [String : Any]
        content.userInfo = userInfo
        
        let id = UUID().uuidString
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        
        return request
    }
    
    private func createFallingBehindNotification(fallingBehindDate: Date?) -> UNNotificationRequest? {
        guard kind == .project, let date = fallingBehindDate, date.timeIntervalSinceNow > 0 else { return nil }
        
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "\(self.title) is falling behind schedule."
        content.sound = UNNotificationSound.default
        
        if kind == .project {
            let remainingSubtasks = numberOfSubtasks() - numberOfCompleteSubtasks()
            content.body = "There are \(remainingSubtasks) tasks remaining."
        }
        
        let userInfo = ["TaskProjectID": id.uuidString, "Type": NotificationType.fallingBehind.rawValue] as [String : Any]
        content.userInfo = userInfo
        
        let id = UUID().uuidString
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        
        return request
    }
    
    func updateCustomTimeNotification(date: Date) {
        let request = self.createCustomTimeNotification(date: date)
        clearAndPostNewLocalNotification(oldId: self.customTimeNotificationID?.uuidString, newRequest: request)
        if let id = request?.identifier {
            self.customTimeNotificationID = UUID(uuidString: id)
        }
    }
    
    func updateDueDateNotification(date: Date?) {
        let request = self.createDueDateNotification(dueDate: date)
        clearAndPostNewLocalNotification(oldId: self.dueDateNotificationID?.uuidString, newRequest: request)
        if let id = request?.identifier {
            self.dueDateNotificationID = UUID(uuidString: id)
        }
    }
    
    func updateFallingBehindNotification(date: Date?) {
        let request = self.createFallingBehindNotification(fallingBehindDate: date)
        clearAndPostNewLocalNotification(oldId: self.fallingBehindNotificationID?.uuidString, newRequest: request)
        if let id = request?.identifier {
            self.fallingBehindNotificationID = UUID(uuidString: id)
        }
    }
    
    func cleanCustomTimeNotification() {
        if let id = self.customTimeNotificationID {
            UNUserNotificationCenter.current().isNotificationPending(id: id.uuidString) { (_, isPending) in
                if !isPending {
                    // Remove existing notification id
                    self.dueDateNotificationID = nil
                }
            }
        }
    }
    
    func cleanAndUpdateDueDateNotification(date: Date?) {
        if let id = self.dueDateNotificationID {
            UNUserNotificationCenter.current().isNotificationPending(id: id.uuidString) { (_, isPending) in
                if isPending {
                    // Update existing notification with new due date
                    self.updateDueDateNotification(date: date)
                } else {
                    // Remove existing notification id
                    self.dueDateNotificationID = nil
                }
            }
        }
    }
    
    func cleanAndUpdateFallingBehindNotification(date: Date?) {
        if let id = self.fallingBehindNotificationID {
            UNUserNotificationCenter.current().isNotificationPending(id: id.uuidString) { (_, isPending) in
                if isPending {
                    // Update existing notification with new due date
                    self.updateFallingBehindNotification(date: date)
                } else {
                    // Remove existing notification id
                    self.fallingBehindNotificationID = nil
                }
            }
        }
    }
    
    func clearAndPostNewLocalNotification(oldId: String?, newRequest: UNNotificationRequest?) {
        if let oldId = oldId {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [oldId])
        }
        
        guard let request = newRequest else { return }
                
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error : Error?) in
            if let theError = error {
                print(theError.localizedDescription)
            }
        }
    }
}

//
//  TaskProjectMO+CoreDataClass.swift
//  Composed
//
//  Created by Patrick Veilleux on 6/24/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//
//

import Foundation
import CoreData
import Combine
import UserNotifications

@objc(TaskProjectMO)
public class TaskProjectMO: NSManagedObject, Identifiable {

    private var childrenSubscription: AnyCancellable!
    private var taskCompleteSubscription: AnyCancellable!
    private var dueDateSubscription: AnyCancellable!
    private var fallingBehindSubscription: AnyCancellable!
    
    @Published var fallingBehindDate: Date?
    
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        setupChildrenSubscription()
        setupTaskCompleteSubscription()
        setupDueDateSubscription()
        setupFallingBehindSubscription()
        cleanNotifications()
        
        self.id = UUID()
        self.dateCreated = Date()
    }
    
    public override func awakeFromFetch() {
        super.awakeFromFetch()
        setupChildrenSubscription()
        setupTaskCompleteSubscription()
        setupDueDateSubscription()
        setupFallingBehindSubscription()
        cleanNotifications()
    }
    
    private func cleanNotifications() {
        cleanCustomTimeNotification()
        cleanAndUpdateDueDateNotification(date: dueDate)
        cleanAndUpdateFallingBehindNotification(date: fallingBehindDate)
    }
    
    private func setupChildrenSubscription() {
        childrenSubscription = self.publisher(for: \.children)
            .dropFirst()
            .sink { (children) in
                RunLoop.main.schedule {
                    self.kind = children.count == 0 ? .task : .project
                    self.updateFallingBehindDate(propagate: true, recursive: true)
                }
            }
    }
    
    private func setupTaskCompleteSubscription() {
        taskCompleteSubscription = self.publisher(for: \.taskComplete)
            .dropFirst()
            .sink { taskComplete in
                DispatchQueue.main.async {
                    self.updateFallingBehindDate(propagate: true, recursive: true)
                }
            }
    }
    
    private func setupDueDateSubscription() {
        dueDateSubscription = self.publisher(for: \.dueDate)
            .dropFirst()
            .removeDuplicates()
            .sink { date in
                self.cleanAndUpdateDueDateNotification(date: date)
                
                RunLoop.main.schedule {
                    self.updateFallingBehindDate(propagate: true, recursive: true)
                }
            }
    }
    
    private func setupFallingBehindSubscription() {
        fallingBehindSubscription = $fallingBehindDate
            .dropFirst()
            .removeDuplicates()
            .sink(receiveValue: { date in
                self.cleanAndUpdateFallingBehindNotification(date: date)
            })
    }
    
    public override func prepareForDeletion() {
        super.prepareForDeletion()
        
        // Clean up any subscriptions to prevent crashing on deletion
        childrenSubscription = nil
        taskCompleteSubscription = nil
        dueDateSubscription = nil
        fallingBehindSubscription = nil
        
        // Remove any pending notifications
        clearAndPostNewLocalNotification(oldId: customTimeNotificationID?.uuidString, newRequest: nil)
        clearAndPostNewLocalNotification(oldId: dueDateNotificationID?.uuidString, newRequest: nil)
        clearAndPostNewLocalNotification(oldId: fallingBehindNotificationID?.uuidString, newRequest: nil)
    }
}

//
//  SceneDelegate.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/13/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
        
        window?.tintColor = Appearance.Color.blue
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.saveContext()
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        let didShowWelcome = UserDefaults.standard.object(forKey: "DidShowWelcomeScreen") as? Bool
        if didShowWelcome != true {
            window?.rootViewController?.present(GuideViewController(), animated: true)
            UserDefaults.standard.set(true, forKey: "DidShowWelcomeScreen")
        }
    }
}


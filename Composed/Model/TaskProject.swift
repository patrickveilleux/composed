//
//  TaskProject.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/16/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import Combine
import UserNotifications

// Conform to Codable for local/network JSON storage
class TaskProject: Identifiable, Codable {
    
    let id: String

    var title: String
    
    let dateCreated: Date
    
    var dueDate: Date?
    
    var notifications: Notifications
        
    var notes: String?
    
    private(set) weak var parentTaskProject: TaskProject?
    
    private(set) weak var rootTaskProject: TaskProject?
    
    var subTaskProjects: [TaskProject] = [] {
        willSet {
            for taskProject in newValue where !subTaskProjects.contains(where: { $0.id == taskProject.id }) {
                configureSubTaskProject(taskProject)
            }
        }
    }
    
    struct Notifications: Codable {
        var whenDueNotificationID: String?
        var customTimeNotificationID: String?
        var fallingBehindNotificationID: String?
        
        enum Types: Int, Codable {
            case whenDue
            case customTime
            case fallingBehind
        }
    }
    
    private var complete: Bool = false
    var isComplete: Bool {
        switch kind {
        case .task:
            return complete
        case .project:
            return taskProgress == 1.0
        }
    }
    
    func setComplete(complete: Bool) {
        self.complete = complete
    }
    
//    var taskSortMethod: SortMethod = SortMethod.Task.title
//    var projectSortMethod: SortMethod = SortMethod.Project.title
    
    init(title: String, id: String, dateCreated: Date) {
        self.title = title
        self.id = id
        self.dateCreated = dateCreated
        self.notifications = Notifications()
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case dateCreated
        case dueDate
        case notes
        case subTaskProjects
        case complete
//        case taskSortMethod
//        case projectSortMethod
        case notifications
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.dateCreated = try container.decode(Date.self, forKey: .dateCreated)
        self.dueDate = try container.decodeIfPresent(Date.self, forKey: .dueDate)
        self.notes = try container.decodeIfPresent(String.self, forKey: .notes)
        self.subTaskProjects = try container.decode([TaskProject].self, forKey: .subTaskProjects)
        self.complete = try container.decode(Bool.self, forKey: .complete)
//        self.taskSortMethod = try container.decodeIfPresent(SortMethod.self, forKey: .taskSortMethod) ?? SortMethod.Task.title
//        self.projectSortMethod = try container.decodeIfPresent(SortMethod.self, forKey: .projectSortMethod) ?? SortMethod.Project.title
        self.notifications = try container.decodeIfPresent(Notifications.self, forKey: .notifications) ?? Notifications()
        
        for subTaskProject in subTaskProjects {
            configureSubTaskProject(subTaskProject)
        }
    }
}

extension TaskProject {
    
    convenience init(title: String) {
        self.init(title: title, id: UUID().uuidString, dateCreated: Date())
    }
    
    func save() {
        if let root = rootTaskProject {
            root.save()
            return
        }
        
        TaskProjectsManager().save(taskProject: self)
    }
    
    enum Kind: String {
        case task = "task"
        case project = "project"
    }
    
    var kind: Kind {
        return subTaskProjects.isEmpty ? .task : .project
    }
    
    var tasks: [TaskProject] {
        var tasks: [TaskProject] = []
        for item in subTaskProjects where item.kind == .task {
            tasks.append(item)
        }
        return tasks
    }
    
    var projects: [TaskProject] {
        var projects: [TaskProject] = []
        for item in subTaskProjects where item.kind == .project {
            projects.append(item)
        }
        return projects
    }
    
    enum ProgressStatus {
        case complete
        case ahead
        case warning
        case behind
        case pastDue
    }
    
    var progressStatus: ProgressStatus {
        if taskProgress == 1.0 {
            return .complete
        }
        
        guard let timeProgress = timeProgress else {
            return .ahead
        }
        
        let nextTaskProgress = Float(numberOfCompleteSubtasks() + 1) / Float(numberOfSubtasks())
        if timeProgress < 0 || (timeProgress >= 1.0 && !isComplete) {
            return .pastDue
        } else if taskProgress >= timeProgress {
            return .ahead
        } else if nextTaskProgress >= timeProgress {
            return .warning
        } else {
            return .behind
        }
    }
    
    var progressStatusPercentage: Float {
        return taskProgress - (timeProgress ?? 0)
    }
    
    var taskProgress: Float {
        return Float(numberOfCompleteSubtasks()) / Float(numberOfSubtasks())
    }
    
    var timeProgress: Float? {
        if let dueDate = self.dueDate {
            let elapsedTime = Date().timeIntervalSince(dateCreated)
            let totalTime = dueDate.timeIntervalSince(dateCreated)
            return Float(elapsedTime / totalTime)
        }
        return nil
    }
    
    var fallingBehindDate: Date? {
        guard let dueDate = self.dueDate, kind == .project else { return nil }
        
        let totalTime = dueDate.timeIntervalSince(dateCreated)
        let fallingBehindInterval = totalTime * Double(taskProgress)
        return Date(timeInterval: fallingBehindInterval, since: dateCreated)
    }
    
    func numberOfSubtasks() -> Int {
        var count = tasks.count
        for subTaskProject in subTaskProjects where subTaskProject.kind == .project {
            count += subTaskProject.numberOfSubtasks()
        }
        return count
    }
    
    func numberOfCompleteSubtasks() -> Int {
        var count = 0
        for task in tasks where task.isComplete == true {
            count += 1
        }
        for subTaskProject in subTaskProjects where subTaskProject.kind == .project {
            count += subTaskProject.numberOfCompleteSubtasks()
        }
        return count
    }
    
    var readableDueDate: String? {
        if let date = dueDate {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .short
            return formatter.string(from: date)
        }
        return nil
    }
    
    // MARK: - NOTIFICATIONS
    
    func createCustomTimeNotification(date: Date) -> UNNotificationRequest {
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "Reminder about your \(kind.rawValue): \(self.title)."
        content.sound = UNNotificationSound.default
        
        if kind == .project {
            let remainingSubtasks = numberOfSubtasks() - numberOfCompleteSubtasks()
            content.body = "There are \(remainingSubtasks) tasks remaining."
        }
        
        let userInfo = ["TaskProject": id, "Type": Notifications.Types.customTime.rawValue] as [String : Any]
        content.userInfo = userInfo
        
        let id = UUID().uuidString
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        
        return request
    }
    
    func createWhenDueNotification(date: Date) -> UNNotificationRequest {
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "The \(kind.rawValue) \"\(self.title)\" is due!"
        content.sound = UNNotificationSound.default
        
        if kind == .project {
            let remainingSubtasks = numberOfSubtasks() - numberOfCompleteSubtasks()
            content.body = "There are \(remainingSubtasks) tasks remaining."
        }
        
        let userInfo = ["TaskProject": id, "Type": Notifications.Types.whenDue.rawValue] as [String : Any]
        content.userInfo = userInfo
        
        let id = UUID().uuidString
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        
        return request
    }
    
    func createFallingBehindNotification(date: Date) -> UNNotificationRequest {
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "\(self.title) is falling behind schedule."
        content.sound = UNNotificationSound.default
        
        if kind == .project {
            let remainingSubtasks = numberOfSubtasks() - numberOfCompleteSubtasks()
            content.body = "There are \(remainingSubtasks) tasks remaining."
        }
        
        let userInfo = ["TaskProject": id, "Type": Notifications.Types.fallingBehind.rawValue] as [String : Any]
        content.userInfo = userInfo
        
        let id = UUID().uuidString
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        
        return request
    }
    
    // MARK: - UPDATING SUBTASKPROJECTS
    
    private func configureSubTaskProject(_ taskProject: TaskProject) {
        taskProject.rootTaskProject = self.rootTaskProject ?? self
        taskProject.parentTaskProject = self
    }
    
    func addSubTaskProject(_ taskProject: TaskProject) {
        configureSubTaskProject(taskProject)
        subTaskProjects.append(taskProject)
    }
    
//    func insertSubTaskProject(_ taskProject: TaskProject, at index: Int) {
//        var tp = taskProject
//        configureSubTaskProject(&tp)
//        subTaskProjects.insert(tp, at: index)
//    }
    
    func removeSubTaskProject(_ taskProject: TaskProject) {
        if let index = subTaskProjects.firstIndex(where: { $0.id == taskProject.id }) {
            subTaskProjects.remove(at: index)
        }
    }
}

//
//  Date+Extensions.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/13/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation

extension Date {
    var zeroSeconds: Date? {
        let calendar = Calendar.current
        return calendar.dateInterval(of: .minute, for: self)?.start
    }

}

extension Optional: Comparable where Wrapped == Date {
    public static func < (lhs: Optional<Wrapped>, rhs: Optional<Wrapped>) -> Bool {
        switch (lhs, rhs) {
        case let (l?, r?):
            return l < r
        case (_?, nil):
            return true
        case (nil, _?):
            return false
        case (nil, nil):
            return true
        }
    }
}

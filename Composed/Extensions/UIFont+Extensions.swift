//
//  UIFont+Extensions.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/21/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    static func preferredFont(for style: TextStyle, weight: Weight) -> UIFont {
        let metrics = UIFontMetrics(forTextStyle: style)
        let desc = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
        let font = UIFont.systemFont(ofSize: desc.pointSize, weight: weight)
        return metrics.scaledFont(for: font)
    }
}

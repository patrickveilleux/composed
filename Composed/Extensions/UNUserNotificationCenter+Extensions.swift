//
//  UNUserNotificationCenter+Extensions.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/15/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import UserNotifications

extension UNUserNotificationCenter {
    func isNotificationPending(id: String, closure: @escaping (String, Bool) -> ()) {
        self.getPendingNotificationRequests { results in
            let isPending = results.contains(where: { $0.identifier == id })
            closure(id, isPending)
        }
    }
}

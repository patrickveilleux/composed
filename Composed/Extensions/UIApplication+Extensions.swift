//
//  UIApplication+Extensions.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/22/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    var icon: UIImage? {
        guard let iconsDictionary = Bundle.main.infoDictionary?["CFBundleIcons"] as? NSDictionary,
            let primaryIconsDictionary = iconsDictionary["CFBundlePrimaryIcon"] as? NSDictionary,
            let iconFiles = primaryIconsDictionary["CFBundleIconFiles"] as? NSArray,
            // First will be smallest for the device class, last will be the largest for device class
            let lastIcon = iconFiles.lastObject as? String,
            let icon = UIImage(named: lastIcon) else {
                return nil
        }
        return icon
    }
}

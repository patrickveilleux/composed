//
//  Bool+Extensions.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/15/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation

extension Bool: Comparable {
    public static func < (lhs: Bool, rhs: Bool) -> Bool {
        return !lhs && rhs
    }
}

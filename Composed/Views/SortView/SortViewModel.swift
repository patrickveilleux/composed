//
//  SortViewModel.swift
//  Composed
//
//  Created by Patrick Veilleux on 4/14/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation

//struct SortMethod: Codable {
//
////    enum Project: String, CaseIterable {
////        case title = "Title"
////        case dueDate = "Due Date"
////        case progress = "Progress"
////        case custom = "Custom"
////    }
////
////    enum Task: String, CaseIterable {
////        case title = "Title"
////        case dueDate = "Due Date"
////        case complete = "Complete"
////        case custom = "Custom"
////    }
//
//    struct Project {
//        static let title = SortMethod(title: "Title")
//        static let dueDate = SortMethod(title: "Due Date")
//        static let progress = SortMethod(title: "Progress")
////        static let custom = SortMethod(title: "Custom", dual: false)
//    }
//
//    struct Task {
//        static let title = SortMethod(title: "Title")
//        static let dueDate = SortMethod(title: "Due Date")
//        static let complete = SortMethod(title: "Complete")
////        static let custom = SortMethod(title: "Custom", dual: false)
//    }
//
//    enum Order: Int, Codable {
//        case high = 0
//        case low = 1
//
//        mutating func toggle() {
//            if self == .high {
//                self = .low
//            } else {
//                self = .high
//            }
//        }
//    }
//
//    var title: String
//    let dual: Bool
//    var order: Order = .low
//
//    init(title: String, dual: Bool = true) {
//        self.title = title
//        self.dual = dual
//    }
//}

//class SortViewModel {
    
//    var kind: TaskProjectSortMethod
    
//    private var taskSort = [
//        SortMethod.Task.title,
//        SortMethod.Task.dueDate,
//        SortMethod.Task.complete,
////        SortMethod.Task.custom
//    ]
//
//    private var projectSort = [
//        SortMethod.Project.title,
//        SortMethod.Project.dueDate,
//        SortMethod.Project.progress,
////        SortMethod.Project.custom
//    ]
    
//    lazy var methods: [SortMethod] = {
//        switch kind {
//        case .projects:
//            return projectSort
//        case .tasks:
//            return taskSort
//        }
//    }()
    
//    var activeMethodIndex: Int = 0
//
//    var activeMethod: SortMethod {
//        if methods.count > activeMethodIndex {
//            return methods[activeMethodIndex]
//        }
//        return methods[0]
//    }
    
//    init(methods: [SortMethod], activeMethod: SortMethod?) {
//        self.kind = kind
        
//        if activeMethod != nil {
//            self.activeMethodIndex = indexOfMethod(sortMethod: activeMethod!) ?? 0
//            methods.replaceSubrange(Range(activeMethodIndex...activeMethodIndex), with: [activeMethod!])
//        }
//    }
    
//    func didSelectMethod(at index: Int) {
//        methods[index].order.toggle()
//        activeMethodIndex = index
//    }
    
//    func indexOfMethod(sortMethod: SortMethod) -> Int? {
//        return methods.firstIndex(where: { $0.title == sortMethod.title })
//    }
//}

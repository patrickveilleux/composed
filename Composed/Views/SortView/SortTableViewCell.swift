//
//  SortTableViewCell.swift
//  Composed
//
//  Created by Patrick Veilleux on 4/13/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

class SortTableViewCell: UITableViewCell {
    
    var method: SortMethod!
    
    func configure() {
        textLabel?.text = method.string
        imageView?.image = method.image
        imageView?.preferredSymbolConfiguration = UIImage.SymbolConfiguration(font: .preferredFont(forTextStyle: .callout), scale: .default)
        imageView?.contentMode = .center
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            textLabel?.textColor = .label
            imageView?.tintColor = Appearance.Color.blue
        } else {
            textLabel?.textColor = Appearance.Color.gray
            imageView?.tintColor = Appearance.Color.gray
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
//        if highlighted {
//            contentView.backgroundColor = Appearance.Color.lightGray.withAlphaComponent(0.25)
//        } else {
//            contentView.backgroundColor = .clear
//        }
    }

}

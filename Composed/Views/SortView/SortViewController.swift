//
//  SortViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 4/13/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine

class SortViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    // MARK: - Properties
    
    private var backgroundView: UIView!
    var contentBaseView: UIView!
    private var contentView: UIView!
    
    private var titleLabel: UILabel!
    private var doneButton: UIButton!
    private var tableView: UITableView!
    let kSORT_CELL_ID = "SortCell"
    
//    var delegate: SortViewDelegate?
//    var model: SortViewModel!
    
    var methods: [SortMethod] = []
    var activeMethod: SortMethod?
    
    var publisher = PassthroughSubject<Action, Never>()
    
    enum Action {
        case didEndWithSortMethod(SortMethod?)
    }
    
    // MARK: - Setup
    
//    init(model: SortViewModel) {
//        super.init(nibName: nil, bundle: nil)
//        self.model = model
//    }
    
    init(methods: [SortMethod], activeMethod: SortMethod?) {
        super.init(nibName: nil, bundle: nil)
        self.methods = methods
        self.activeMethod = activeMethod
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        setupViews()
    }
    
    func setupViews() {
        
        if backgroundView == nil {
            backgroundView = UIView()
            backgroundView.backgroundColor = Appearance.Color.shadowBlack.withAlphaComponent(0.25)
            view.addSubview(backgroundView)
        }
        
        if contentBaseView == nil {
            contentBaseView = UIView()
            contentBaseView.layer.shadowColor = Appearance.Color.shadowBlack.cgColor
            contentBaseView.layer.shadowOffset = CGSize(width: 0, height: 3)
            contentBaseView.layer.shadowOpacity = Appearance.shadowOpacity
            contentBaseView.layer.shadowRadius = 1.5
            view.addSubview(contentBaseView)
        }
        
        if contentView == nil {
            contentView = UIView()
            contentView.backgroundColor = Appearance.Color.nearWhite
            contentView.layer.cornerRadius = 15
            contentView.layer.masksToBounds = true
            contentBaseView.addSubview(contentView)
        }
        
        if titleLabel == nil {
            titleLabel = UILabel()
            titleLabel.text = "Sort"
            titleLabel.font = UIFont.preferredFont(for: .title2, weight: .bold)
            contentView.addSubview(titleLabel)
        }
        
        if doneButton == nil {
            doneButton = UIButton()
            doneButton.tintColor = Appearance.Color.darkGray
            doneButton.setImage(UIImage(systemName: "xmark"), for: .normal)
            let symbolConfig = UIImage.SymbolConfiguration(pointSize: 17, weight: .regular)
            doneButton.setPreferredSymbolConfiguration(symbolConfig, forImageIn: .normal)
            doneButton.addTarget(self, action: #selector(didTapSortViewDoneButton(sender:)), for: .touchUpInside)
            contentView.addSubview(doneButton)
        }
        
        if tableView == nil {
            tableView = UITableView()
            tableView.isScrollEnabled = false
            tableView.backgroundColor = .clear
            tableView.register(SortTableViewCell.self, forCellReuseIdentifier: kSORT_CELL_ID)
            tableView.dataSource = self
            tableView.delegate = self
            if let activeMethod = activeMethod,
                let indexPath = indexPathForMethod(sortMethod: activeMethod) {
                
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
            
            contentView.addSubview(tableView)
        }
        
        layoutSubviews()
    }
    
    func layoutSubviews() {
        
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        let blurConstraints = [
            backgroundView.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(blurConstraints)
        
        contentBaseView.translatesAutoresizingMaskIntoConstraints = false
        let contentBaseConstraints = [
            contentBaseView.heightAnchor.constraint(equalToConstant: calculatedHeight()),
            contentBaseView.widthAnchor.constraint(equalToConstant: 300),
            contentBaseView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            contentBaseView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ]
        NSLayoutConstraint.activate(contentBaseConstraints)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        let contentConstraints = [
            contentView.topAnchor.constraint(equalTo: contentBaseView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: contentBaseView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: contentBaseView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: contentBaseView.bottomAnchor)
        ]
        NSLayoutConstraint.activate(contentConstraints)
        
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        let buttonConstraints = [
            doneButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            doneButton.heightAnchor.constraint(equalToConstant: 30),
            doneButton.widthAnchor.constraint(equalToConstant: 30),
            doneButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 25)
        ]
        NSLayoutConstraint.activate(buttonConstraints)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let labelConstraints = [
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            titleLabel.heightAnchor.constraint(equalToConstant: 30),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 25),
            titleLabel.trailingAnchor.constraint(equalTo: doneButton.leadingAnchor, constant: 8)
        ]
        NSLayoutConstraint.activate(labelConstraints)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let tableConstraints = [
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 60),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ]
        NSLayoutConstraint.activate(tableConstraints)
    }
    
    private func calculatedHeight() -> CGFloat {
        return 60 + (CGFloat(methods.count) * 52) + 12
    }
    
    @objc func didTapSortViewDoneButton(sender: UIButton) {
//        delegate?.sortViewDidTapDoneButton(self)
        publisher.send(.didEndWithSortMethod(activeMethod))
    }
    
    func indexPathForMethod(sortMethod: SortMethod) -> IndexPath? {
        if let index = methods.firstIndex(where: { $0.string == sortMethod.string }) {
            return IndexPath(row: index, section: 0)
        } else {
            return nil
        }
    }
}

extension SortViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return methods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kSORT_CELL_ID, for: indexPath) as! SortTableViewCell
        cell.method = methods[indexPath.row]
        cell.configure()
        return cell
    }
}

extension SortViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SortTableViewCell {
//            model.didSelectMethod(at: indexPath.row)
            activeMethod = methods[indexPath.row]
            cell.configure()
        }
    }
}

//protocol SortViewDelegate {
//    func sortViewDidTapDoneButton(_ sortView: SortView)
//}

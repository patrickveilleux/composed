//
//  TaskListEditButton.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/20/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

@IBDesignable
class TaskListEditButton: UIButton {

    private var backgroundLayer: CAShapeLayer!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = nil
        
        if backgroundLayer == nil {
            backgroundLayer = CAShapeLayer()
            let rect = bounds.insetBy(dx: Appearance.lineWidth / 2, dy: Appearance.lineWidth / 2)
            backgroundLayer.path = UIBezierPath(roundedRect: rect, cornerRadius: bounds.height / 2.0).cgPath
            backgroundLayer.fillColor = Appearance.Color.gray.cgColor
            
            backgroundLayer.shadowColor = Appearance.Color.shadowBlack.cgColor
            backgroundLayer.shadowOffset = Appearance.shadowOffset
            backgroundLayer.shadowOpacity = Appearance.shadowOpacity
            backgroundLayer.shadowRadius = Appearance.shadowRadius

            layer.insertSublayer(backgroundLayer, at: 0)
        }
    }

}

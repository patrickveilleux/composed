//
//  DatePickerViewController.swift
//  Composed
//
//  Created by Patrick Veilleux on 6/8/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

class DatePickerViewController: UIAlertController {

    var datePicker: UIDatePicker = UIDatePicker()

    override func viewDidLoad() {
        super.viewDidLoad()

        datePicker.datePickerMode = .dateAndTime
//        datePicker.addTarget(self, action: #selector(datePickerDidChangeValue(_:)), for: .valueChanged)
        view.addSubview(datePicker)

        datePicker.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            datePicker.topAnchor.constraint(equalTo: view.topAnchor, constant: 32),
            datePicker.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            datePicker.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            datePicker.heightAnchor.constraint(equalToConstant: 216)
        ])

        view.translatesAutoresizingMaskIntoConstraints = false
        let height = 320 + (actions.count - 1) * 60
        view.heightAnchor.constraint(equalToConstant: CGFloat(height)).isActive = true
    }

//    @objc func datePickerDidChangeValue(_ datePicker: UIDatePicker) {
//        print("date changed")
////        date = datePicker.date
//    }
}

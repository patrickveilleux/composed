//
//  SegmentedControl.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/16/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit


//class SegmentedControl: UIControl {
//
//    private var backgroundLayer: CAShapeLayer!
//    private var selectionLayer: CAShapeLayer!
//
//    private var button1: UIButton!
//    private var button2: UIButton!
//
//    var selectedSegment: Int {
//        return selectedSegmentIndex
//    }
//    private var selectedSegmentIndex: Int = 0 {
//        didSet {
//            sendActions(for: .valueChanged)
//        }
//    }
//
//    @IBInspectable var firstTitle: String = "First" {
//        didSet {
//            button1?.setTitle(firstTitle, for: .normal)
//        }
//    }
//    @IBInspectable var secondTitle: String = "Second" {
//        didSet {
//            button2?.setTitle(secondTitle, for: .normal)
//        }
//    }
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        backgroundColor = nil
//
//        if backgroundLayer == nil {
//            backgroundLayer = CAShapeLayer()
//            backgroundLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.height / 2.0).cgPath
//            backgroundLayer.shadowColor = Appearance.Color.shadowBlack.cgColor
//            backgroundLayer.shadowPath = backgroundLayer.path
//            backgroundLayer.shadowOffset = Appearance.shadowOffset
//            backgroundLayer.shadowOpacity = Appearance.shadowOpacity
//            backgroundLayer.shadowRadius = Appearance.shadowRadius
//
//            layer.insertSublayer(backgroundLayer, at: 0)
//        }
//
//        if selectionLayer == nil {
//            selectionLayer = CAShapeLayer()
//            updateSelectionLayerPath(animated: false)
//
//            layer.insertSublayer(selectionLayer, above: backgroundLayer)
//        }
//
//        if button1 == nil {
//            button1 = UIButton(frame: CGRect(x: 0, y: 0, width: bounds.width / 2.0, height: bounds.height))
//            button1.titleLabel?.font = Appearance.Font.captionFont
//            button1?.setTitle(firstTitle, for: .normal)
//            button1.backgroundColor = .clear
//            button1.addTarget(self, action: #selector(didTapFirstButton), for: .touchUpInside)
//            addSubview(button1)
//        }
//
//        if button2 == nil {
//            button2 = UIButton(frame: CGRect(x: bounds.width / 2.0, y: 0, width: bounds.width / 2.0, height: bounds.height))
//            button2.titleLabel?.font = Appearance.Font.captionFont
//            button2?.setTitle(secondTitle, for: .normal)
//            button2.backgroundColor = .clear
//            button2.addTarget(self, action: #selector(didTapSecondButton), for: .touchUpInside)
//            addSubview(button2)
//        }
//
//        backgroundLayer.fillColor = Appearance.Color.lightGray.cgColor
//        selectionLayer.fillColor = Appearance.Color.darkGray.cgColor
//        button1.setTitleColor(Appearance.Color.nearWhite, for: .normal)
//        button2.setTitleColor(Appearance.Color.nearWhite, for: .normal)
//    }
//
//    private func updateSelectionLayerPath(animated: Bool) {
//        if selectionLayer != nil {
//            let rect = CGRect(x: bounds.width / 2.0 * CGFloat(selectedSegmentIndex), y: 0, width: bounds.width / 2.0, height: bounds.height)
//            let newPath = UIBezierPath(roundedRect: rect, cornerRadius: bounds.height / 2.0).cgPath
//
//            CATransaction.begin()
//            CATransaction.setAnimationDuration(0.25)
//            CATransaction.setAnimationTimingFunction(CAMediaTimingFunction.init(name: .easeOut))
//
//            let pathAnimation = CABasicAnimation(keyPath: #keyPath(CAShapeLayer.path))
//            pathAnimation.fromValue = selectionLayer.path
//            pathAnimation.toValue = newPath
//
//            selectionLayer.path = newPath
//            selectionLayer.add(pathAnimation, forKey: #keyPath(CAShapeLayer.path))
//
//            CATransaction.commit()
//        }
//    }
//
//    @objc private func didTapFirstButton() {
//        selectSegment(index: 0, animated: true)
//        let generator = UISelectionFeedbackGenerator()
//        generator.selectionChanged()
//    }
//
//    @objc private func didTapSecondButton() {
//        selectSegment(index: 1, animated: true)
//        let generator = UISelectionFeedbackGenerator()
//        generator.selectionChanged()
//    }
//
//    func selectSegment(index: Int, animated: Bool) {
//        selectedSegmentIndex = index
//        updateSelectionLayerPath(animated: animated)
//    }
//}

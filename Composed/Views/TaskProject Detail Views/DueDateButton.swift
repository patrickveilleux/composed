//
//  DueDateButton.swift
//  Composed
//
//  Created by Patrick Veilleux on 7/11/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine

class DueDateButton: UIButton {

    private var datePicker: UIDatePicker?
    private var toolbarView: UIView?
    
    var date: Date? {
        didSet {
            datePicker?.date = date ?? Date()
        }
    }

    var publisher = PassthroughSubject<Action, Never>()

    enum Action {
        case dateChanged(Date)
        case didTapDoneButton
        case didTapDeleteButton
    }
    
    override var inputView: UIView? {
        get {
            return datePicker
        }
        set {
            if let newPicker = newValue as? UIDatePicker {
                self.datePicker = newPicker
            }
        }
    }

    override var inputAccessoryView: UIView? {
        get {
            return toolbarView
        }
        set {
            self.toolbarView = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupDatePicker()
        setupToolbarView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupDatePicker()
        setupToolbarView()
    }
    
    private func setupDatePicker() {
        if datePicker == nil {
            let dp = UIDatePicker()
            dp.datePickerMode = .dateAndTime
            dp.minuteInterval = 5
            dp.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
            inputView = dp
        }
    }

    private func setupToolbarView() {
        if toolbarView == nil {
            let doneButton = UIButton()
            doneButton.setTitle("Done", for: .normal)
            doneButton.backgroundColor = Appearance.Color.blue
            doneButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
            doneButton.addTarget(self, action: #selector(didTapDoneButton), for: .touchUpInside)
            doneButton.layer.cornerRadius = 20
            doneButton.layer.masksToBounds = true

            let deleteButton = UIButton()
            deleteButton.setTitle("Delete", for: .normal)
            deleteButton.backgroundColor = Appearance.Color.red
            deleteButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .subheadline)
            deleteButton.addTarget(self, action: #selector(didTapDeleteButton), for: .touchUpInside)
            deleteButton.layer.cornerRadius = 20
            deleteButton.layer.masksToBounds = true

            let tb = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 56))
            tb.addSubview(deleteButton)
            tb.addSubview(doneButton)

            doneButton.translatesAutoresizingMaskIntoConstraints = false
            deleteButton.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                deleteButton.leadingAnchor.constraint(equalTo: tb.safeAreaLayoutGuide.leadingAnchor, constant: 8),
                deleteButton.heightAnchor.constraint(equalToConstant: 40),
                deleteButton.centerYAnchor.constraint(equalTo: tb.centerYAnchor),
                
                deleteButton.trailingAnchor.constraint(equalTo: doneButton.leadingAnchor, constant: -8),
                
                doneButton.heightAnchor.constraint(equalToConstant: 40),
                doneButton.trailingAnchor.constraint(equalTo: tb.safeAreaLayoutGuide.trailingAnchor, constant: -8),
                doneButton.centerYAnchor.constraint(equalTo: tb.centerYAnchor),
                
                doneButton.widthAnchor.constraint(equalTo: deleteButton.widthAnchor)
            ])

            inputAccessoryView = tb
        }
    }
    
    @objc private func datePickerValueChanged(sender: UIDatePicker) {
        publisher.send(.dateChanged(sender.date))
    }

    @objc private func didTapDoneButton() {
        publisher.send(.didTapDoneButton)
        resignFirstResponder()
    }

    @objc private func didTapDeleteButton() {
        publisher.send(.didTapDeleteButton)
    }

//    override func becomeFirstResponder() -> Bool {
//        super.becomeFirstResponder()
//        return true
//    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
}

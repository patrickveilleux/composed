//
//  Appearance.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/16/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1)
    }
}

struct Appearance {
    
    struct Color {
        
        static var blue: UIColor {
            return UIColor(red: 16, green: 145, blue: 245)
        }
        
        static var red: UIColor {
            return UIColor(red: 240, green: 22, blue: 81)
        }
        
        static var orange: UIColor {
            return UIColor(red: 244, green: 124, blue: 69)
        }
        
        static var yellow: UIColor {
            return UIColor(red: 242, green: 196, blue: 34)
        }
        
        static var green: UIColor {
            return UIColor(red: 1, green: 216, blue: 71)
        }
        
        static var purple: UIColor {
            return UIColor(red: 93, green: 59, blue: 238)
        }
        
        static var darkGray: UIColor {
            return UIColor(named: "Dark Gray") ?? UIColor(white: 0.2, alpha: 1.0)
        }
        
        static var gray: UIColor {
            return UIColor(named: "Gray") ?? UIColor(white: 0.5, alpha: 1.0)
        }
        
        static var lightGray: UIColor {
            return UIColor(named: "Light Gray") ?? UIColor(white: 0.8, alpha: 1.0)
        }
        
        static var nearWhite: UIColor {
            return UIColor(named: "Near White") ?? UIColor(white: 0.95, alpha: 1.0)
        }
        
        static var white: UIColor {
            return UIColor(named: "White") ?? UIColor(white: 1.0, alpha: 1.0)
        }
        
        static var shadowBlack: UIColor {
            return .clear
        }
    }
    
    static var shadowOffset: CGSize {
        return CGSize(width: 0.0, height: 1.0)
    }
    
    static var shadowRadius: CGFloat {
        return 0.5
    }
    
    static var shadowOpacity: Float {
        return 0.4
    }
    
    static var lineWidth: CGFloat {
        return 2.5
    }
    
    static var cornerRadius: CGFloat {
        return 20
    }
    
    struct Font {
        
        enum Weight: String {
            case thin = "Montserrat-Thin"
            case thinItalic = "Montserrat-ThinItalic"
            case extraLight = "Montserrat-ExtraLight"
            case extraLightItalic = "Montserrat-ExtraLightItalic"
            case light = "Montserrat-Light"
            case lightItalic = "Montserrat-LightItalic"
            case regular = "Montserrat-Regular"
            case italic = "Montserrat-Italic"
            case medium = "Montserrat-Medium"
            case mediumItalic = "Montserrat-MediumItalic"
            case semiBold = "Montserrat-SemiBold"
            case semiBoldItalic = "Montserrat-SemiBoldItalic"
            case bold = "Montserrat-Bold"
            case boldItalic = "Montserrat-BoldItalic"
            case extraBold = "Montserrat-ExtraBold"
            case extraBoldItalic = "Montserrat-ExtraBoldItalic"
            case black = "Montserrat-Black"
            case blackItalic = "Montserrat-BlackItalic"
        }
        
        static func standardFont(weight: Weight, size: CGFloat) -> UIFont {
            return UIFont(name: weight.rawValue, size: size)!
        }
        
        static var titleFont: UIFont {
            return standardFont(weight: .semiBold, size: 22)
        }
        
        static var headerFont: UIFont {
            return standardFont(weight: .semiBold, size: 20)
        }
        
        static var cellTitleFont: UIFont {
            return standardFont(weight: .medium, size: 15)
        }
        
        static var cellDetailFont: UIFont {
            return standardFont(weight: .medium, size: 10)
        }
        
        static var captionFont: UIFont {
            return standardFont(weight: .semiBold, size: 10)
        }
    }
}

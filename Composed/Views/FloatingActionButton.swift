//
//  FloatingActionButton.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/16/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

@IBDesignable
class FloatingActionButton: UIButton {

//    private var shadowLayer: CAShapeLayer!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = Appearance.Color.blue
        tintColor = Appearance.Color.nearWhite
        layer.cornerRadius = bounds.size.width / 2
        layer.masksToBounds = true
        
//        if shadowLayer == nil {
//            shadowLayer = CAShapeLayer()
//            shadowLayer.path = UIBezierPath(ovalIn: bounds).cgPath
//            shadowLayer.fillColor = Appearance.Color.blue.cgColor
//
//            shadowLayer.shadowColor = Appearance.Color.shadowBlack.cgColor
//            shadowLayer.shadowPath = shadowLayer.path
//            shadowLayer.shadowOffset = Appearance.shadowOffset
//            shadowLayer.shadowOpacity = Appearance.shadowOpacity
//            shadowLayer.shadowRadius = Appearance.shadowRadius
//
//            layer.insertSublayer(shadowLayer, at: 0)
//        }
    }
}

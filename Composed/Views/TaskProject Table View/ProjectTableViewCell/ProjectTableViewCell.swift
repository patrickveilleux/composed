//
//  ProjectTableViewCell.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/16/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine

class ProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var detailsLabel: UILabel!
    
    @IBOutlet weak var statusIndicator: ProjectStatusIndicatorView!
    
    @IBOutlet weak var taskProgressIndicator: ProgressIndicatorView!
    
    @IBOutlet weak var timeProgressIndicator: ProgressIndicatorView!
    
//    var model: ProjectTableViewCellViewModel?
    
    var taskProject: TaskProjectMO!
    
    private var subscriptions = Set<AnyCancellable>()
    
    func configure() {
        guard let project = taskProject else { return }
        
//        project.publisher(for: \.title)
//            .sink { text in
//                self.titleLabel.attributedText = self.getFormattedText(string: text, completion: project.isComplete)
//            }.store(in: &subscriptions)
//
//        project.publisher(for: \.dueDate)
//            .sink(receiveValue: { date in
//                self.detailsLabel.text = self.getDetailsText(date: date)
//            })
//            .store(in: &subscriptions)
        
//        project.publisher(for: \.isComplete)
//            .sink { isComplete in
//                self.titleLabel.textColor = isComplete ? Appearance.Color.gray : .label
//                self.detailsLabel.textColor = isComplete ? Appearance.Color.gray : .label
//                self.titleLabel.attributedText = self.getFormattedText(string: project.title, completion: isComplete)
//                self.detailsLabel.text = self.getDetailsText(date: project.dueDate)
//            }.store(in: &subscriptions)
        
        titleLabel.textColor = project.isComplete ? Appearance.Color.gray : .label
        detailsLabel.textColor = project.isComplete ? Appearance.Color.gray : .label
        titleLabel.attributedText = self.getFormattedText(string: project.title, completion: project.isComplete)
        detailsLabel.text = self.getDetailsText(date: project.dueDate)
        
        statusIndicator.statusColor = statusIndicatorColor
        taskProgressIndicator.barColors = taskProgressIndicatorColors
        timeProgressIndicator.barColors = timeProgressIndicatorColors
        taskProgressIndicator.barBackgroundColor = progressIndicatorBackgroundColor
        timeProgressIndicator.barBackgroundColor = progressIndicatorBackgroundColor
        taskProgressIndicator.progress = taskProgress
        timeProgressIndicator.progress = timeProgress
    }
    
    private func getDetailsText(date: Date?) -> String {
        let taskCount = taskProject.numberOfSubtasks()
        
        var details = "\(String(taskCount)) task"
        
        if taskCount > 1 {
            details += "s"
        }
                
        if let date = date {
            let formatter = DateFormatter()
            formatter.doesRelativeDateFormatting = true
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            details += " due at \(formatter.string(from: date))"
        }
                
        return details
    }
    
    private func getFormattedText(string: String, completion: Bool) -> NSAttributedString {
        let attrString = NSMutableAttributedString(string: string)
        if completion {
            attrString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.thick.rawValue, range: NSMakeRange(0, attrString.length))
        }
        return attrString
    }
    
    private var statusIndicatorColor: UIColor {
        switch taskProject.progressStatus {
        case .complete:
            return Appearance.Color.blue
        case .ahead:
            return Appearance.Color.green
        case .warning:
            return Appearance.Color.yellow
        case .behind:
            return Appearance.Color.red
        case .pastDue:
            return Appearance.Color.red
        }
    }
    
    private var taskProgressIndicatorColors: [UIColor] {
        if taskProject.progressStatus == .complete {
            return [Appearance.Color.blue, Appearance.Color.purple]
        } else {
            return [Appearance.Color.green, Appearance.Color.blue]
        }
    }
    
    private var timeProgressIndicatorColors: [UIColor] {
        if taskProject.progressStatus == .complete {
            return [Appearance.Color.blue, Appearance.Color.purple]
        } else if taskProject.timeProgress == nil {
            return [Appearance.Color.lightGray, Appearance.Color.lightGray]
        } else {
            return [Appearance.Color.orange, Appearance.Color.red]
        }
    }
    
    private var _progressIndicatorBackgroundColor: UIColor?
    private var progressIndicatorBackgroundColor: UIColor? {
        set {
            _progressIndicatorBackgroundColor = newValue
        }
        get {
            if taskProject.progressStatus == .complete {
                return .clear
            } else {
                return _progressIndicatorBackgroundColor
            }
        }
    }
    
    private var taskProgress: Float {
        return taskProject.taskProgress
    }
    
    private var timeProgress: Float {
        if taskProject.isComplete || taskProject.timeProgress ?? 1 < 0.0 {
            return 1.0
        } else {
            return taskProject.timeProgress ?? 1.0
        }
    }
}

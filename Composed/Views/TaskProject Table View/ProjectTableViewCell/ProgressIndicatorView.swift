//
//  ProgressIndicatorView.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/17/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

@IBDesignable
class ProgressIndicatorView: UIView {

    @IBInspectable var progress: Float = 0.25 {
        didSet {
            setNeedsLayout()
        }
    }
    
    var barColors: [UIColor] = [Appearance.Color.green]
    var barBackgroundColor: UIColor?

    private var backgroundLayer: CAShapeLayer!
    private var progressLayer: CAShapeLayer!
    private var gradientLayer: CAGradientLayer!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = .clear
                
        if backgroundLayer == nil {
            backgroundLayer = CAShapeLayer()
            backgroundLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.height / 2.0).cgPath
//            backgroundLayer.shadowColor = Appearance.Color.shadowBlack.cgColor
//            backgroundLayer.shadowPath = backgroundLayer.path
//            backgroundLayer.shadowOffset = Appearance.shadowOffset
//            backgroundLayer.shadowOpacity = Appearance.shadowOpacity
//            backgroundLayer.shadowRadius = Appearance.shadowRadius

            layer.insertSublayer(backgroundLayer, at: 0)
//            layer.mask = backgroundLayer
        }
        
        if progressLayer == nil {
            progressLayer = CAShapeLayer()
            progressLayer.fillColor = UIColor.black.cgColor
            progressLayer.strokeColor = UIColor.clear.cgColor
        }
        
        if gradientLayer == nil {
            gradientLayer = CAGradientLayer()
            gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
            gradientLayer.frame = bounds
            gradientLayer.mask = progressLayer
            layer.insertSublayer(gradientLayer, above: backgroundLayer)
        }
        
        backgroundLayer.fillColor = barBackgroundColor?.cgColor
        gradientLayer.colors = barColors.map({ $0.cgColor })
        progressLayer.path = UIBezierPath(roundedRect: rectForProgress(progress: progress), cornerRadius: bounds.height / 2.0).cgPath
    }
    
    func rectForProgress(progress: Float) -> CGRect {
        let boundedProgress = progress < 0 ? 0 : progress > 1 ? 1 : progress
        let height = bounds.height
        let width = height + (bounds.width - height) * CGFloat(boundedProgress)
        return CGRect(x: 0, y: 0, width: width, height: bounds.height)
    }
}

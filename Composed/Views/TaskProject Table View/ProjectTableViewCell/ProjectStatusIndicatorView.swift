//
//  ProjectStatusIndicatorView.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/17/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

@IBDesignable
class ProjectStatusIndicatorView: UIView {

    private var shadowLayer: CAShapeLayer!
    
    @IBInspectable var statusColor: UIColor = Appearance.Color.red {
        didSet {
            setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = nil
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.height / 2.0).cgPath

            shadowLayer.shadowColor = Appearance.Color.shadowBlack.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = Appearance.shadowOffset
            shadowLayer.shadowOpacity = Appearance.shadowOpacity
            shadowLayer.shadowRadius = Appearance.shadowRadius

            layer.insertSublayer(shadowLayer, at: 0)
        }
        
        shadowLayer.fillColor = statusColor.cgColor
    }
}

//
//  ProjectTableViewCellViewModel.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/31/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import Foundation
import UIKit
import Combine

class ProjectTableViewCellViewModel {
    
    private var project: TaskProjectMO
    
    @Published var title: NSAttributedString!
    
    var details: String {
        let taskCount = project.numberOfSubtasks()
        
        var details = "\(String(taskCount)) task"
        
        if taskCount > 1 {
            details += "s"
        }
                
        if let date = project.dueDate {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            details += " due at \(formatter.string(from: date))"
        }
        
        return details
    }
    
    var statusIndicatorColor: UIColor {
        switch project.progressStatus {
        case .complete:
            return Appearance.Color.blue
        case .ahead:
            return Appearance.Color.green
        case .warning:
            return Appearance.Color.yellow
        case .behind:
            return Appearance.Color.red
        case .pastDue:
            return Appearance.Color.red
        }
    }
    
    var taskProgressIndicatorColors: [UIColor] {
        if project.progressStatus == .complete {
            return [Appearance.Color.blue, Appearance.Color.purple]
        } else {
            return [Appearance.Color.green, Appearance.Color.blue]
        }
    }
    
    var timeProgressIndicatorColors: [UIColor] {
        if project.progressStatus == .complete {
            return [Appearance.Color.blue, Appearance.Color.purple]
        } else if project.timeProgress == nil {
            return [Appearance.Color.lightGray, Appearance.Color.lightGray]
        } else {
            return [Appearance.Color.orange, Appearance.Color.red]
        }
    }
    
    private var _progressIndicatorBackgroundColor: UIColor?
    var progressIndicatorBackgroundColor: UIColor? {
        set {
            _progressIndicatorBackgroundColor = newValue
        }
        get {
            if project.progressStatus == .complete {
                return .clear
            } else {
                return _progressIndicatorBackgroundColor
            }
        }
    }
    
    var taskProgress: Float {
        return project.taskProgress
    }
    
    var timeProgress: Float {
        if project.isComplete || project.timeProgress ?? 1 < 0.0 {
            return 1.0
        } else {
            return project.timeProgress ?? 1.0
        }
    }
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(project: TaskProjectMO) {
        self.project = project
        
        project.publisher(for: \.isComplete)
            .sink { isComplete in
                self.title = self.getFormattedText(string: project.title, completion: isComplete)
            }.store(in: &subscriptions)
    }
    
    private func getFormattedText(string: String, completion: Bool) -> NSAttributedString {
        let attrString = NSMutableAttributedString(string: string)
        if completion {
            attrString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.thick.rawValue, range: NSMakeRange(0, attrString.length))
        }
        return attrString
    }
}

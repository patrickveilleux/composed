//
//  TaskProjectTableHeaderView.swift
//  Composed
//
//  Created by Patrick Veilleux on 4/15/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine

class TaskProjectTableHeaderView: UITableViewHeaderFooterView {

    @Published var title: String?
    
    var sortButtonClosure: (() -> ())?
    
    private var subscriber: AnyCancellable?
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    func commonInit() {
        let label = UILabel()
        label.font = UIFont.preferredFont(for: .title1, weight: .bold)
        subscriber = $title.sink { (title) in
            label.text = title
        }
        addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        let labelConstraints = [
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            label.heightAnchor.constraint(equalToConstant: 30)
        ]
        NSLayoutConstraint.activate(labelConstraints)
        
        let filterButton = UIButton()
        filterButton.tintColor = .label
        filterButton.setImage(UIImage(systemName: "arrow.up.arrow.down"), for: .normal)
        let symbolConfig = UIImage.SymbolConfiguration(pointSize: 17, weight: .regular)
        filterButton.setPreferredSymbolConfiguration(symbolConfig, forImageIn: .normal)
        addSubview(filterButton)
        
        filterButton.addTarget(self, action: #selector(didTapSortButton), for: .touchUpInside)
        
        filterButton.translatesAutoresizingMaskIntoConstraints = false
        let buttonConstraints = [
            filterButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            filterButton.heightAnchor.constraint(equalToConstant: 40),
            filterButton.widthAnchor.constraint(equalToConstant: 40),
            filterButton.centerYAnchor.constraint(equalTo: label.centerYAnchor)
        ]
        NSLayoutConstraint.activate(buttonConstraints)
    }
    
    @objc func didTapSortButton() {
        sortButtonClosure?()
    }
}

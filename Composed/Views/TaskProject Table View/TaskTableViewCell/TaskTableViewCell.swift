//
//  TaskTableViewCell.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/16/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit
import Combine

class TaskTableViewCell: UITableViewCell {
    
    @IBOutlet weak var completionButton: TaskCompletionButton!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var detailsLabel: UILabel!
    
    var taskCompletionUpdateClosure: ((Bool) -> ())?
    
    var taskProject: TaskProjectMO?
    
    private var subscriptions = Set<AnyCancellable>()
    
    func configure() {
        guard let task = taskProject else { return }
        
        titleTextField.attributedText = getFormattedText(string: task.title)
        titleTextField.textColor = task.isComplete ? Appearance.Color.gray : .label
        detailsLabel.textColor = task.isComplete ? Appearance.Color.gray : .label
        detailsLabel.text = getDetailsText(date: task.dueDate)
        detailsLabel.isHidden = task.dueDate == nil
        completionButton.isCompleted = task.isComplete
    }
    
    private func getDetailsText(date: Date?) -> String {
        if let date = date {
            let formatter = DateFormatter()
            formatter.doesRelativeDateFormatting = true
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            return formatter.string(from: date)
        } else {
            return ""
        }
    }
    
    private func getFormattedText(string: String) -> NSAttributedString {
        let attrString = NSMutableAttributedString(string: string)
        if taskProject?.isComplete ?? false {
            attrString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.thick.rawValue, range: NSMakeRange(0, attrString.length))
        }
        return attrString
    }
    
    @IBAction func didTapCompletionButton(_ sender: TaskCompletionButton) {
        taskProject?.taskComplete = sender.isCompleted
        configure()
        taskCompletionUpdateClosure?(sender.isCompleted)
    }
}

//protocol TaskTableViewCellDelegate {
//    func completionButtonTapped(cell: TaskTableViewCell)
//}

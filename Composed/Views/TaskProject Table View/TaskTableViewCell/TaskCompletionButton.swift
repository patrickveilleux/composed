//
//  TaskCompletionButton.swift
//  Composed
//
//  Created by Patrick Veilleux on 3/16/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import UIKit

@IBDesignable
class TaskCompletionButton: UIButton {

    private var shadowLayer: CAShapeLayer!
    
    let visualSize = CGSize(width: 30, height: 30)
    
    var isCompleted: Bool = false {
        didSet {
            shadowLayer?.fillColor = fillColorForCompletedState()
            shadowLayer?.strokeColor = strokeColorForCompletedState()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        addTarget(self, action: #selector(didTap), for: .touchUpInside)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = nil
        
        if shadowLayer == nil {
            let visualOrigin = CGPoint(x: (bounds.width - visualSize.width) / 2.0, y: (bounds.height - visualSize.height) / 2.0)
            let visualRect = CGRect(origin: visualOrigin, size: visualSize)
            let rect = visualRect.insetBy(dx: Appearance.lineWidth / 2, dy: Appearance.lineWidth / 2)
            
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: rect, cornerRadius: bounds.height / 2.0).cgPath
            shadowLayer.strokeColor = strokeColorForCompletedState()
            shadowLayer.lineWidth = Appearance.lineWidth
            shadowLayer.fillColor = fillColorForCompletedState()

            shadowLayer.shadowPath = shadowLayer.path?.copy(strokingWithWidth: Appearance.lineWidth, lineCap: .round, lineJoin: .round, miterLimit: 0)
            shadowLayer.shadowColor = Appearance.Color.shadowBlack.cgColor
            shadowLayer.shadowOffset = Appearance.shadowOffset
            shadowLayer.shadowOpacity = Appearance.shadowOpacity
            shadowLayer.shadowRadius = Appearance.shadowRadius

            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    @objc private func didTap() {
        isCompleted.toggle()
        
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred()
    }
    
    private func fillColorForCompletedState() -> CGColor {
        return isCompleted ? Appearance.Color.blue.cgColor : UIColor.clear.cgColor
    }
    
    private func strokeColorForCompletedState() -> CGColor {
        return isCompleted ? Appearance.Color.blue.cgColor : Appearance.Color.red.cgColor
    }
}

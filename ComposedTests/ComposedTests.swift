//
//  ComposedTests.swift
//  ComposedTests
//
//  Created by Patrick Veilleux on 3/13/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import XCTest
@testable import Composed

class ComposedTests: XCTestCase {

    func testProjectMeasurable() {
        let task1 = TaskProject(title: "Task1")
        let task2 = TaskProject(title: "Task2")
        let task3 = TaskProject(title: "Task3")
        task1.setComplete(complete: true)
        
        let project = TaskProject(title: "Project")
        project.subTaskProjects = [task1, task2, task3]
        
        XCTAssertEqual(project.taskProgress, Float(1.0/3.0))
        XCTAssert(!project.isComplete)
        
        task2.setComplete(complete: true)
        task3.setComplete(complete: true)
        
        project.subTaskProjects = [task1, task2, task3]
        
        XCTAssertEqual(project.taskProgress, Float(1))
        XCTAssert(project.isComplete)
    }
    
    func testSubTaskProjectCount() {
        let task1 = TaskProject(title: "Task1")
        let task2 = TaskProject(title: "Task2")
        let task3 = TaskProject(title: "Task3")
        task1.setComplete(complete: true)
        
        let task4 = TaskProject(title: "Task4")
        let task5 = TaskProject(title: "Task5")
        let task6 = TaskProject(title: "Task6")
        let task7 = TaskProject(title: "Task7")
        task4.setComplete(complete: true)
        task5.setComplete(complete: true)
        
        let subProject = TaskProject(title: "SubProject")
        subProject.subTaskProjects = [task4, task5, task6, task7]
        
        let project = TaskProject(title: "Project")
        project.subTaskProjects = [task1, task2, task3, subProject]
        
        XCTAssertEqual(project.numberOfSubtasks(), 7)
    }
    
    func testSubTaskProjectCompleteCount() {
        let task1 = TaskProject(title: "Task1")
        let task2 = TaskProject(title: "Task2")
        let task3 = TaskProject(title: "Task3")
        task1.setComplete(complete: true)
        
        let task4 = TaskProject(title: "Task4")
        let task5 = TaskProject(title: "Task5")
        let task6 = TaskProject(title: "Task6")
        let task7 = TaskProject(title: "Task7")
        task4.setComplete(complete: true)
        task5.setComplete(complete: true)
        
        let subProject = TaskProject(title: "SubProject")
        subProject.subTaskProjects = [task4, task5, task6, task7]
        
        let project = TaskProject(title: "Project")
        project.subTaskProjects = [task1, task2, task3, subProject]
        
        XCTAssertEqual(project.numberOfCompleteSubtasks(), 3)
    }
}

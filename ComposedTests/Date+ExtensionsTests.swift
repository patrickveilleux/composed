//
//  Date+ExtensionsTests.swift
//  ComposedTests
//
//  Created by Patrick Veilleux on 7/15/20.
//  Copyright © 2020 Patrick Veilleux. All rights reserved.
//

import XCTest

class Date_ExtensionsTests: XCTestCase {

    func testComparison() throws {
        let now = Date()
        let soon = Date().addingTimeInterval(5000)
        let none: Date? = nil
        
        XCTAssert((now < soon) == true)
        XCTAssert((soon < now) == false)
        XCTAssert((now < none) == true)
        XCTAssert((none < now) == false)
        XCTAssert((none < none) == true)
    }
}
